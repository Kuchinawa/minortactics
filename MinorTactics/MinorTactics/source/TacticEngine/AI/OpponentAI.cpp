#include "OpponentAI.h"

#include "..\GameObjects\AIComponent.h"
#include "..\GameObjects\CharStatsComponent.h"

using namespace TacticEngine;

OpponentAI::OpponentAI()
{

}

OpponentAI::OpponentAI(std::vector<GameObject*>* enemyCharacters, 
                       std::vector<GameObject*>* playerCharacters, 
                       level* level,
                       float meleeDistanceMOD,
                       float rangedDistanceMOD,
                       float healerDistanceMOD,
                       float meleeAttackMOD,
                       float rangedAttackMOD,
                       float healerHealMOD,
                       float receiveHealMOD,
                       float healerWarningMOD,
                       float healerDangerMOD,
                       float giveHealMOD,
                       float healingThreshold)
{
    enemyCharacters_ = enemyCharacters;
    playerCharacters_ = playerCharacters;
    level_ = level;

    meleeDistanceMOD_ = meleeDistanceMOD;
    rangedDistanceMOD_ = rangedDistanceMOD;
    healerDistanceMOD_ = healerDistanceMOD;

    meleeAttackMOD_ = meleeAttackMOD;
    rangedAttackMOD_ = rangedAttackMOD;
    healerHealMOD_ = healerHealMOD;

    receiveHealMOD_ = receiveHealMOD;
    healerWarningMOD_ = healerWarningMOD;
    healerDangerMOD_ = healerDangerMOD;
    giveHealMOD_ = giveHealMOD;
    healingThreshold_ = healingThreshold;
}

void OpponentAI::update()
{
    //Make sure only one character is active at a time
    for(GameObject* character : *enemyCharacters_)
    {
        if(character->getComponent<CharStatsComponent>()->isAlive)
        {
            if(character->getComponent<AIComponent>()->active)
            {
                return;
            }

            if(character->getComponent<AIComponent>()->active == false && character->getComponent<AIComponent>()->done == false)
            {
                character->getComponent<AIComponent>()->active = true;
                decideActions(*character);
                return;
            }
        }
    }
}

void OpponentAI::decideActions(GameObject& character)
{
    switch(character.getComponent<CharStatsComponent>()->classType)
    {
    case Warrior:
        calculateWarriorMovement(character);
        break;
    case Archer:
        calculateRangeMovement(character);
        break;
    case Priest:
        calculateHealerMovement(character);
        break;
    }
}

void OpponentAI::calculateWarriorMovement(GameObject& character)
{
    glm::vec2 dimensions = level_->getTerrainDimensions();

    glm::vec2 bestTile = vec2(0,0);
    float bestScore = -10000;
    std::vector<ASPoint*> bestRoute;
    bool performingAction = false;
    GameObject* actionTarget = nullptr;

    for(int x = 0; x < dimensions.x; x++)
    {
        for(int y = 0; y < dimensions.y; y++)
        {
            if(character.getComponent<TransformComponent>()->get2DLocation() == vec2(x,y) || !level_->pathFinder.getOccupied(x, y))
            {
                float score = 0;
                bool attacked = false;
                GameObject* posActionTarget = nullptr;

                std::vector<ASPoint*> currentRoute = level_->pathFinder.aStar(character.getComponent<TransformComponent>()->get2DLocation().x,
                    character.getComponent<TransformComponent>()->get2DLocation().y, 
                    x, 
                    y, 
                    character.getComponent<CharStatsComponent>()->jumpHeight);

                //Subtract turns to point
                score -= glm::ceil((float)currentRoute.size() / (float)character.getComponent<CharStatsComponent>()->speed) * meleeDistanceMOD_;

                //Add melee attack possibility bonus

                for(GameObject* playerCharacter : *playerCharacters_)
                {
                    if(attacked == false)
                    {
                        //if possible target is alive
                        if(playerCharacter->getComponent<CharStatsComponent>()->isAlive)
                        {   //if the target is in a tile next to this tile
                            if((glm::abs(x - playerCharacter->getComponent<TransformComponent>()->get2DLocation().x) == 1 && 
                                glm::abs(y - playerCharacter->getComponent<TransformComponent>()->get2DLocation().y) == 0)
                                || 
                                (glm::abs(x - playerCharacter->getComponent<TransformComponent>()->get2DLocation().x) == 0 && 
                                glm::abs(y - playerCharacter->getComponent<TransformComponent>()->get2DLocation().y) == 1))
                            {   
                                //If not too low/high
                                if(abs(level_->pathFinder.getPointY(x,y) - 
                                    level_->pathFinder.getPointY(playerCharacter->getComponent<TransformComponent>()->get2DLocation().x, 
                                    playerCharacter->getComponent<TransformComponent>()->get2DLocation().y)) < 2)
                                {   //Add as possible target
                                    score += meleeAttackMOD_;
                                    attacked = true;
                                    posActionTarget = playerCharacter;
                                }
                            }
                        }
                    }
                }

                //If low health and healer is in range add heal bonus
                if(character.getComponent<CharStatsComponent>()->currentHealth < (character.getComponent<CharStatsComponent>()->maxHealth * 0.2f))
                {
                    for(GameObject* enemyCharacter : *enemyCharacters_)
                    {   //if within healing range of a priest
                        if(enemyCharacter->getComponent<CharStatsComponent>()->classType == Priest)
                        {   
                            if(distanceMh(glm::vec2(x,y), 
                                enemyCharacter->getComponent<TransformComponent>()->get2DLocation()) <= 
                                enemyCharacter->getComponent<CharStatsComponent>()->spellRange)
                            {
                                score += receiveHealMOD_;
                            }
                        }
                    }
                }

                if(bestScore < score)
                {
                    bestScore = score;
                    bestTile = glm::vec2(x,y);
                    bestRoute = currentRoute;

                    if(attacked)
                    {
                        performingAction = true;
                        actionTarget = posActionTarget;
                    }
                    else
                    {
                        performingAction = false;
                        actionTarget = nullptr;
                    }
                }
            }
        }
    }
    character.getComponent<MovementComponent>()->startMoving(bestRoute);

    if(performingAction)
    {
        character.getComponent<AIComponent>()->planAction(Attacking, actionTarget);
    }
}

void OpponentAI::calculateRangeMovement(GameObject& character)
{
    glm::vec2 dimensions = level_->getTerrainDimensions();

    glm::vec2 bestTile = vec2(0,0);
    float bestScore = -10000;
    std::vector<ASPoint*> bestRoute;
    bool performingAction = false;
    GameObject* actionTarget = nullptr;

    for(int x = 0; x < dimensions.x; x++)
    {
        for(int y = 0; y < dimensions.y; y++)
        {
            if(character.getComponent<TransformComponent>()->get2DLocation() == vec2(x,y) || !level_->pathFinder.getOccupied(x, y))
            {
                float score = 0;
                GameObject* posActionTarget = nullptr;
                bool attacked = false;
                std::vector<ASPoint*> currentRoute = level_->pathFinder.aStar(character.getComponent<TransformComponent>()->get2DLocation().x,
                    character.getComponent<TransformComponent>()->get2DLocation().y, 
                    x, 
                    y, 
                    character.getComponent<CharStatsComponent>()->jumpHeight);

                //Subtract turns to point
                score -= glm::ceil((float)currentRoute.size() / (float)character.getComponent<CharStatsComponent>()->speed) * rangedDistanceMOD_;

                //Add ranged attack possibility bonus
                for(GameObject* playerCharacter : *playerCharacters_)
                {
                    if(attacked == false)
                    {   //if target is alive
                        if(playerCharacter->getComponent<CharStatsComponent>()->isAlive)
                        {   //if within attack range
                            if(distanceMh(vec2(x,y), 
                                playerCharacter->getComponent<TransformComponent>()->get2DLocation()) <= 
                                character.getComponent<CharStatsComponent>()->attackRange)
                            {   //Add as possible target
                                score += rangedAttackMOD_;
                                attacked = true;
                                posActionTarget = playerCharacter;
                            }
                        }
                    }
                }

                //If low health and healer is in range add heal bonus
                if(character.getComponent<CharStatsComponent>()->currentHealth < 
                    (character.getComponent<CharStatsComponent>()->maxHealth * 0.2f))
                {
                    for(GameObject* enemyCharacter : *enemyCharacters_)
                    {
                        if(enemyCharacter->getComponent<CharStatsComponent>()->classType == Priest)
                        {
                            if(distanceMh(vec2(x,y), 
                                enemyCharacter->getComponent<TransformComponent>()->get2DLocation()) <= 
                                enemyCharacter->getComponent<CharStatsComponent>()->spellRange)
                            {
                                score += receiveHealMOD_;
                            }
                        }
                    }
                }

                if(bestScore < score)
                {
                    bestScore = score;
                    bestTile = glm::vec2(x,y);
                    bestRoute = currentRoute;

                    if(attacked)
                    {
                        performingAction = true;
                        actionTarget = posActionTarget;
                    }
                    else
                    {
                        performingAction = false;
                        actionTarget = nullptr;
                    }
                }
            }
        }
    }
    character.getComponent<MovementComponent>()->startMoving(bestRoute);

    if(performingAction)
    {
        character.getComponent<AIComponent>()->planAction(Attacking, actionTarget);
    }
}

void OpponentAI::calculateHealerMovement(GameObject& character)
{
    glm::vec2 dimensions = level_->getTerrainDimensions();

    glm::vec2 bestTile = vec2(0,0);
    float bestScore = -10000;
    std::vector<ASPoint*> bestRoute;
    bool performingAction = false;
    GameObject* actionTarget = nullptr;
    ActionTypes actionType;

    for(int x = 0; x < dimensions.x; x++)
    {
        for(int y = 0; y < dimensions.y; y++)
        {
            if(character.getComponent<TransformComponent>()->get2DLocation() == vec2(x,y) || !level_->pathFinder.getOccupied(x, y))
            {
                float score = 0;
                GameObject* posActionTarget = nullptr;
                bool healed = false;
                ActionTypes posActionType;

                std::vector<ASPoint*> currentRoute = level_->pathFinder.aStar(character.getComponent<TransformComponent>()->get2DLocation().x,
                    character.getComponent<TransformComponent>()->get2DLocation().y, 
                    x, 
                    y, 
                    character.getComponent<CharStatsComponent>()->jumpHeight);

                //Subtract turns to point
                score -= glm::ceil((float)currentRoute.size() / (float)character.getComponent<CharStatsComponent>()->speed) * meleeDistanceMOD_;

                //Subtract danger modifier if within attack range
                for(GameObject* playerCharacter : *playerCharacters_)
                {
                    //score += (level_->pathFinder.aStar(character.getComponent<TransformComponent>()->get2DLocation().x,
                    //    character.getComponent<TransformComponent>()->get2DLocation().y, 
                    //    x, 
                    //    y, 
                    //    character.getComponent<CharStatsComponent>()->jumpHeight).size() / 
                    //    (character.getComponent<CharStatsComponent>()->speed)) * healerWarningMOD_;

                    if(distanceMh(vec2(x,y), 
                        playerCharacter->getComponent<TransformComponent>()->get2DLocation()) <= 
                        playerCharacter->getComponent<CharStatsComponent>()->attackRange)
                    {
                        score -= healerDangerMOD_;
                    }
                }

                //If friendly is below 80% and in range add heal mod
                for(GameObject* fCharacter : *enemyCharacters_)
                {
                    if(!healed) //Can only heal once a turn
                    {
                        //If life below treshhold
                        if(fCharacter->getComponent<CharStatsComponent>()->currentHealth < 
                            (fCharacter->getComponent<CharStatsComponent>()->maxHealth * healingThreshold_))
                        {

                            if(distanceMh(vec2(x,y), 
                                fCharacter->getComponent<TransformComponent>()->get2DLocation()) <= 
                                character.getComponent<CharStatsComponent>()->spellRange)
                            {
                                score += giveHealMOD_;
                                posActionType = Healing;
                                posActionTarget = fCharacter;
                                healed = true;
                            }
                        }
                    }
                }

                if(bestScore < score)
                {
                    bestScore = score;
                    bestTile = glm::vec2(x,y);
                    bestRoute = currentRoute;

                    if(healed)
                    {
                        performingAction = true;
                        actionTarget = posActionTarget;
                        actionType = posActionType;
                    }
                    else
                    {
                        performingAction = false;
                        actionTarget = nullptr;
                    }
                }
            }
        }
    }

    character.getComponent<MovementComponent>()->startMoving(bestRoute);

    if(performingAction)
    {
        character.getComponent<AIComponent>()->planAction(Healing, actionTarget);
    }
}

bool OpponentAI::finished()
{
    //Check if all the characters have completed their turn
    for(GameObject* character : *enemyCharacters_)
    {
        if(character->getComponent<CharStatsComponent>()->isAlive)
        {
            if(character->getComponent<AIComponent>()->done == false)
            {
                return false;
            }
        }
    }
    return true;
}