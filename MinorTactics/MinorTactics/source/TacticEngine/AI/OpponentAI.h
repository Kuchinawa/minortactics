#pragma once

#include "..\GameObjects\GameObject.h"
#include "PathFinder.h"
#include "..\level.h"

namespace TacticEngine
{
    class OpponentAI
    {
    public:

        OpponentAI();

        OpponentAI(std::vector<GameObject*>* enemyCharacters, 
            std::vector<GameObject*>* playerCharacters, 
            level* level,
            float meleeDistanceMOD = 1.0f,
            float rangedDistanceMOD = 1.0f,
            float healerDistanceMOD = 1.0f,
            float meleeAttackMOD = 5.0f,
            float rangedAttackMOD = 5.0f,
            float healerHealMOD = 5.0f,
            float receiveHealMOD = 10.0f,
            float healerWarningMOD = 5.0f,
            float healerDangerMOD = 10.0f,
            float giveHealMOD = 10.0f,
            float healingThreshold = 0.80f);
        
        void update();

        void decideActions(GameObject& character);

        void calculateWarriorMovement(GameObject& character);
        void calculateRangeMovement(GameObject& character);
        void calculateHealerMovement(GameObject& character);

        bool finished();

    private:

        float meleeDistanceMOD_;
        float meleeAttackMOD_;

        float rangedDistanceMOD_;
        float rangedAttackMOD_;

        float healerDistanceMOD_;
        float healerHealMOD_;
        
        float receiveHealMOD_;
        float healerWarningMOD_;
        float healerDangerMOD_;
        float giveHealMOD_;
        float healingThreshold_;

        std::vector<GameObject*>* enemyCharacters_;
        std::vector<GameObject*>* playerCharacters_;
        level* level_;

        float distanceMh(glm::vec2 point1, glm::vec2 point2)
        {
            return(abs(point1.x - point2.x) + abs(point1.y - point2.y));
        }
    };
}