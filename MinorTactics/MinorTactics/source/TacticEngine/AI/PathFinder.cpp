#include "Pathfinder.h"

using namespace TacticEngine;

PathFinder::PathFinder()
{

}

void PathFinder::addTerrain(GameObject* gObject)
{
    glm::vec3 location = gObject->getComponent<TransformComponent>()->location;

    //If new point
    if(!pointExists(location.x, location.z))
    {
        grid[location.x][location.z] = new ASPoint(location.x, 1, location.z, true);
    }
    else
    {
        grid[location.x][location.z]->growY();
    }
}

void PathFinder::removeTerrain(GameObject* gObject)
{
    glm::vec3 location = gObject->getComponent<TransformComponent>()->location;

    if(pointExists(location.x, location.z))
    {
        //If not the last terrain block
        if(grid[location.x][location.z]->getY() > 1)
        {
            grid[location.x][location.z]->shrinkY();
        }
        else
        {
            //Else remove point
            grid[location.x].erase(location.z);

            if(grid[location.x].empty())
            {
                grid.erase(location.x);
            }
        }
    }
}

std::vector<ASPoint*> PathFinder::aStar(int x1, int z1, int x2, int z2, int jumpHeight)
{
    std::vector<ASPoint*> path;

    //Define points to work with
    ASPoint* start = getPoint(x1, z1);
    ASPoint* end   = getPoint(x2, z2);
    ASPoint* current = nullptr;
    ASPoint* child;

    //Define the open and close list
    std::list<ASPoint*> openList;
    std::vector<ASPoint*> closedList;
    //std::vector<ASPoint*>::iterator i;

    unsigned int n = 0;

    //Add the start point to the openList
    openList.push_back(start);
    start->opened = true;

    //Loop until the end point or 50 iterations
    while(n == 0 || (current != end && n < 50))
    {
        //Look for the smallest F value in the openList and make it the current point
        for(ASPoint* point : openList)
        {
            if (point == openList.front() || (point)->getFScore() <= current->getFScore())
            {
                current = (point);
            }
        }

        //Stop if we reached the end
        if(current == end)
        {
            break;
        }

        //Remove the current point from the openList
        openList.remove(current);
        current->opened = false;

        //Add the current point to the closedList
        closedList.push_back(current);
        current->closed = true;

        //Get all current's adjecent walkable points
        for(int x = -1 ; x < 2; x++)
        {
            for(int z = -1; z < 2; z++)
            {

                //If its a diagonal point then pass, REMOVE FOR DIAGONAL MOVEMENT
                if(x != 0 && z != 0)
                {
                    continue;
                }

                //If its current point then pass
                if(x == 0 && z == 0)
                {
                    continue;
                }

                //Get this point
                child = getPoint(current->getX() + x, current->getZ() + z);

                //If it's closed or not walkable pass
                if(child->closed || !child->walkable || child->getY() == 0)
                {
                    continue;
                }

                //If its to high to jump pass
                if(abs(child->getY() - current->getY()) > jumpHeight)
                {
                    continue;
                }


                //If we are at a corner, ADD FOR DIAGONAL MOVEMENT
                //if(x != 0 && z != 0)
                //{
                //    //If the next horizontal point is not walkable or in the closed list then pass
                //    if (!pointIsWalkable(current->getY(), current->getZ() + z) || getPoint(current->getX(), current->getZ() + z)->closed)
                //    {
                //        continue;
                //    }

                //    //If the next vertical point is not walkable or in the closed list then pass
                //    if (!pointIsWalkable(current->getX() + x, current->getZ()) || getPoint(current->getX() + x, current->getZ())->closed)
                //    {
                //        continue;
                //    }
                //}

                //If its allready in the openList
                if(child->opened)
                {
                    //If it has a worse g score than the one that pass through the current point
                    //then its path is improved when its parent is the current point
                    if(child->getGScore() > child->getGScore(current))
                    {
                        //Change its parent and g score
                        child->setParent(current);
                        child->computeScores(end);
                    }
                }
                else
                {
                    //Add it to the openList with current point as parent
                    openList.push_back(child);
                    child->opened = true;

                    //Compute its g, h and f score
                    child->setParent(current);
                    child->computeScores(end);
                }
            }
        }
        n++;
    }

    //Reset
    for(ASPoint* point : openList)
    {
        point->opened = false;
    }
    for(ASPoint* point : closedList)
    {
        point->closed = false;
    }

    //Resolve the path starting from the end point
    while(current->hasParent() && current != start)
    {
        path.push_back(current);
        current = current -> getParent();
    }

    return path;
}

ASPoint* PathFinder::getPoint(int x, int z)
{
    if(pointExists(x, z))
    {
        return grid[x][z];
    }
    else
    {
        //std::cout << "ERROR: failed to gather point " << x << "x" << z << " on grid" << std::endl;
        return new ASPoint(0, 0, 0, false);
    }
}

bool PathFinder::pointExists(int x, int z)
{
    return (grid.count(x) != 0 && grid[x].count(z) != 0);
}

bool PathFinder::pointIsWalkable(int x, int z)
{
    return (pointExists(x, z) && grid[x][z]->walkable);
}

std::vector<std::vector<int>> PathFinder::getGrid()
{
    std::vector<std::vector<int>> returnGrid;

    returnGrid.resize(grid.size());

    for(std::pair<int, std::map<int, ASPoint*>> row : grid)
    {
        for(std::pair<int, ASPoint*> point : row.second)
        {
            returnGrid.at(row.first).push_back(point.second->getY());
        }
    }

    return returnGrid;
}

float PathFinder::getPointY(float x, float z)
{
    if(pointExists(x, z))
    {
        return grid[x][z]->getYf();
    }
    else
    {
        //std::cout << "ERROR: point doesn't exist " << x << "x" << z << " on grid" << std::endl;
        return 0;
    }
}

bool PathFinder::getOccupied(float x, float z)
{
    if(pointExists(x, z))
    {
        return grid[x][z]->occupied;
    }
    else
    {
        //std::cout << "ERROR: point doesn't exist " << x << "x" << z << " on grid" << std::endl;
        return false;
    }
}

void PathFinder::setOccupied(float x, float z, bool occupied)
{
    if(pointExists(x, z))
    {
        grid[x][z]->occupied = occupied;
    }
}