#pragma once

#include <map>
#include <vector>
#include <iostream>

#include <glm/glm.hpp>

#include "asPoint.h"

#include "..\GameObjects\GameObject.h"
#include "..\GameObjects\TransformComponent.h"

namespace TacticEngine
{
    class PathFinder
    {
    public:
        PathFinder();

        //Calculate the best route between two points with a max jump height
        //Returns a vector with target points (inverted)
        std::vector<ASPoint*> aStar(int x1, int y1, int x2, int y2, int jumpHeight);
        void addTerrain(GameObject* gObject);
        void removeTerrain(GameObject* gObject);

        //Get a 2D representation of the terrain
        std::vector<std::vector<int>> getGrid();

        //Get point z value
        float getPointY(float x, float z);

        //Get/set if point is occupied
        void setOccupied(float x, float z, bool occupied);
        bool getOccupied(float x, float z);

    private:
        ASPoint* getPoint(int x, int y);

        bool pointExists(int x, int y);
        bool pointIsWalkable(int x, int y);

        std::map<int, std::map<int, ASPoint*>> grid;
    };
}