#include "asPoint.h"

using namespace TacticEngine;

ASPoint::ASPoint()
{
    parent_ = nullptr;
    closed = false;
    opened = false;

    x = y = z = f = g = h = 0;
}

ASPoint::ASPoint(int x, int y, int z, bool walkable)
{
    //ASPoint();

    parent_ = nullptr;
    closed = false;
    opened = false;
    occupied = false;

    this->f = 0;
    this->g = 0;
    this->h = 0;

    this->x = x;
    this->y = y;
    this->z = z;
    this->walkable = walkable;
}

glm::vec3 ASPoint::getPosition()
{
    return glm::vec3((float)x, (float)y, (float)z);
}

ASPoint* ASPoint::getParent()
{
    return parent_;
}

void ASPoint::setParent(ASPoint *p)
{
    parent_ = p;
}

void ASPoint::growY()
{
    y++;
}
void ASPoint::shrinkY()
{
    y--;
}


int ASPoint::getX()
{
    return x;
}

int ASPoint::getY()
{
    return y;
}

int ASPoint::getZ()
{
    return z;
}

float ASPoint::getXf()
{
    return (float)x;
}

float ASPoint::getYf()
{
    return (float)y;
}

float ASPoint::getZf()
{
    return (float)z;
}

int ASPoint::getGScore(ASPoint *p)
{
    return p->g + ((x == p->x || z == p->z) ? 10 : 14);
}

int ASPoint::getHScore(ASPoint *p)
{
    return (abs(p->x - x) + abs(p->z - z)) * 10;
}

int ASPoint::getGScore()
{
    return g;
}

int ASPoint::getHScore()
{
    return h;
}

int ASPoint::getFScore()
{
    return f;
}

void ASPoint::computeScores(ASPoint *end)
{
    g = getGScore(parent_);
    h = getHScore(end);
    f = g + h;
}

bool ASPoint::hasParent()
{
    return parent_ != nullptr;
}