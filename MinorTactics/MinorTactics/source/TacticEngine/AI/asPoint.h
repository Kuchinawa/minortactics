#pragma once

#include <glm\glm.hpp>

namespace TacticEngine
{
    class ASPoint
    {
    public:
        bool walkable;
        bool closed;
        bool opened;
        bool occupied;

        ASPoint();
        ASPoint(int x, int y, int z, bool walkable);

        glm::vec3 getPosition();
        ASPoint* getParent();

        void setParent(ASPoint *p);

        void growY();
        void shrinkY();

        int getX();
        int getY();
        int getZ();

        float getXf();
        float getYf();
        float getZf();

        //The movement cost from start to this point, calculated through p
        int getGScore(ASPoint *p);
        //The (estimated) movement cost from this point to the end, calculated through p
        int getHScore(ASPoint *p);

        //The movement cost from start to this point
        int getGScore();
        //The (estimated) movement cost from this point to the end
        int getHScore();
        //The total score of this point (G + H)
        int getFScore();

        void computeScores(ASPoint *end);

        bool hasParent();

    private:
        ASPoint* parent_;

        int x;
        int y;
        int z;

        unsigned int f;
        unsigned int g;
        unsigned int h;
    };
}