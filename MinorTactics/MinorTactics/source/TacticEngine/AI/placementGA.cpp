#include <algorithm>
#include <chrono>
#include <iostream>

#include <fstream>

#include "placementGA.h"

using namespace TacticEngine;

placementGA::placementGA(
    std::vector<BattleClassType> unitTypes, 
    std::vector<vec2> opponents,
    std::vector<std::vector<int>> area, 
    int nrOfGenerations, 
    int populationSize, 
    int crossoverPercent, 
    int mutatepercent,
    vec2 minPosition, 
    vec2 maxPosition,
    float heightMOD,
    float healerRangeMOD,
    float enemyDistanceMODM,
    float enemyDistanceMODR,
    float enemyDistanceMODH)
{
    heightMOD_ = heightMOD;
    healerRangeMOD_ = healerRangeMOD;
    enemyDistanceMODM_ = enemyDistanceMODM;
    enemyDistanceMODR_ = enemyDistanceMODR;
    enemyDistanceMODH_ = enemyDistanceMODH;

    unitTypes_ = unitTypes;
    opponents_ = opponents;
    area_ = area;
    nrOfGenerations_ = nrOfGenerations;
    crossoverPercent_ = crossoverPercent;
    mutatePercent_ = mutatepercent;

    currentGeneration_ = 0;

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    randomMt19937 = std::mt19937(seed);

    //populationSize_ has to be even
    if(populationSize % 2 == 0)
        populationSize_ = populationSize;
    else
        populationSize_ = ++populationSize;

    //Ensure min is min and max is max corners
    minPosition_ = vec2(min(minPosition.x, maxPosition.x), min(minPosition.y, maxPosition.y));
    maxPosition_ = vec2(max(minPosition.x, maxPosition.x), max(minPosition.y, maxPosition.y));
}


void placementGA::runGA()
{
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    std::vector<vec2> possiblePositions;

    //Generate possible positions
    for(int x = minPosition_.x; x < maxPosition_.x + 1; x++)
        for(int y = minPosition_.y; y < maxPosition_.y + 1; y++)
            possiblePositions.push_back(vec2(x,y));

    //Generate a starting generation at random
    for(int i = 0; i < populationSize_; i++)
    {
        population_.push_back(randomIndividual(possiblePositions, unitTypes_.size()));
    }

    //Evaluate the fitness of all individuals
    for(int i = 0; i < populationSize_; i++)
    {
        fitness_.push_back(positioningFitness(population_[i], unitTypes_, opponents_, area_));
    }

    elitism();

    for(int i = 0; i < nrOfGenerations_; i++)
    {
        select(); //Select new parents
        crossover(); //Switch chromosomes
        mutate(possiblePositions); //Mutate random individuals
        calcFitness(); //Recalculate the fitness scores
        ++currentGeneration_;
        report(); //Report on the current scores
        elitism(); //Save best individual
    }

    bestPositions = population_.back();

    std::cout <<"----------" << std::endl << "Final score: " << std::endl;
    report();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    typedef std::chrono::duration<int,std::milli> millisecs_t ;
    millisecs_t duration( std::chrono::duration_cast<millisecs_t>(end-start) ) ;
    std::cout << "Completed in: " << duration.count() << " milliseconds.\n" ;
}

std::vector<vec2> placementGA::randomIndividual(std::vector<vec2> possiblePositions, int unitAmount)
{
    std::vector<vec2> chosen;
    for(int i = 0; i < unitAmount; i++)
    {   //choose an element and swap that element to the end of the vector
        //by decreasing the max rand we ensure the picked elements will not be chosen again.
        std::uniform_int_distribution<int> dist(0, possiblePositions.size() - i - 1);
        int index = dist(randomMt19937);
        chosen.push_back(possiblePositions[index]);
        std::swap(possiblePositions[index], possiblePositions[possiblePositions.size() - i - 1]);
    }
    return chosen;
}

void placementGA::select()
{
    float sum = 0.0f;

    //Determine the total fitness of the population
    for(int i = 0; i < populationSize_; i++)
    {
        sum += fitness_[i];
    }

    std::uniform_int_distribution<int> dist(0, sum);
    population newPopulation;
    std::vector<float> newFitness;

    for(int i = 0; i < populationSize_; i++)
    {
        //Grab a random individual by producing a random number between 0 and sum
        //Then subtract from random untill it has reached 0, grab that individual
        int random = dist(randomMt19937);
        int current = 0;

        do {
            random -= fitness_[current];
            ++current;
        } while(random > 0);
        newPopulation.push_back(population_[current - 1]);
        newFitness.push_back(fitness_[current - 1]);
    }

    population_ = newPopulation;
}

void placementGA::crossover()
{
    std::uniform_int_distribution<int> crossDist(0, 999);
    std::uniform_int_distribution<int> pointDist(1, unitTypes_.size());

    //For every couple
    for(int i = 0; i < populationSize_; i += 2)
    {
        //Check if they fall into the mating percentage
        int rCross = crossDist(randomMt19937);
        if(rCross < crossoverPercent_)
        {
            //Swap the elements until the crossover point
            int rPoint = pointDist(randomMt19937);
            for(int j = 0; j < rPoint; j++)
            {
                //If the to be swapped chromosome is allready in the target individual skip
                if(std::find(population_[i].begin(), population_[i].end(), population_[i+1][j]) == population_[i].end() && 
                    std::find(population_[i+1].begin(), population_[i+1].end(), population_[i][j]) == population_[i+1].end())
                {
                    std::swap(population_[i][j], population_[i+1][j]);
                }
            }
        }
    }
}

void placementGA::mutate(std::vector<vec2> possiblePositions)
{
    std::uniform_int_distribution<int> mutateDist(0, 999);
    std::uniform_int_distribution<int> posDist(0, possiblePositions.size() - unitTypes_.size() - 1);


    for(int i = 0; i < populationSize_; i++)
    {
        for(int j = 0; j < unitTypes_.size(); j++)
        {
            int rMutate = mutateDist(randomMt19937);
            if(rMutate < mutatePercent_)
            {
                //Move all the current elements to the end
                for(int k = 0; k < unitTypes_.size(); k++)
                {
                    int index = std::distance(possiblePositions.begin(), std::find(possiblePositions.begin(), possiblePositions.end(), population_[i][k]));
                    std::swap(possiblePositions[index], possiblePositions[possiblePositions.size() - k - 1]);
                }

                //Select a new position to mutate the chromosome to
                int pos = posDist(randomMt19937);
                population_[i][j] = possiblePositions[pos];
            }
        }
    }
}

void placementGA::calcFitness()
{
    fitness_.clear();
    //Evaluate the fitness of all individuals
    for(int i = 0; i < populationSize_; i++)
    {
        fitness_.push_back(positioningFitness(population_[i], unitTypes_, opponents_, area_));
    }
}

void placementGA::report()
{
    float average = 0.0f;
    float sum = 0.0f;
    float best = 0.0f;

    for(int i = 0; i < fitness_.size(); i++)
    {
        sum += fitness_[i];
    }

    average = sum / (float)populationSize_;

    std::cout 
        << "Generation:    " << currentGeneration_ << std::endl
        << "Average Score: " << average << std::endl
        << "Best score:    " << *std::max_element(fitness_.begin(), fitness_.end()) << std::endl;

    std::ofstream reportFile;
    reportFile.open("fitnessreport.txt", std::ios_base::app);
    reportFile << currentGeneration_ << "\t" << *std::max_element(fitness_.begin(), fitness_.end()) << std::endl;
    reportFile.close();
}

void placementGA::elitism()
{
    int bestIndex = std::distance(fitness_.begin(), std::max_element(fitness_.begin(), fitness_.end()));

    //If new best is not equal to the current best its higher
    if(fitness_[bestIndex] != fitness_[populationSize_ - 1])
    {   //replace with new best
        fitness_[populationSize_ - 1] = fitness_[bestIndex];
        population_[populationSize_ - 1] = population_[bestIndex];
    }
    else
    {   //replace worst with best
        int worstIndex = std::distance(fitness_.begin(), std::min_element(fitness_.begin(), fitness_.end()));
        fitness_[worstIndex] = fitness_[populationSize_ - 1];
        population_[worstIndex] = population_[populationSize_ - 1];
    }
}

float placementGA::positioningFitness(std::vector<vec2> positions,
                                      std::vector<BattleClassType> unitTypes,
                                      std::vector<vec2> opponents,
                                      std::vector<std::vector<int>> area)
                                      //float maxDistance)
{
    float fitness = 0.0f;

    float maxDistance = distanceMh(vec2(1.0f, 1.0f), vec2(area.size(), area.back().size()));

    std::vector<vec2> ranged;
    std::vector<vec2> melee;
    std::vector<vec2> healers;

    for(int i = 0; i < positions.size(); i++)
    {
        switch(unitTypes[i])
        {
        case Archer: 
            ranged.push_back(positions[i]);
            break;
        case Warrior:
            melee.push_back(positions[i]);
            break;
        case Priest:
            healers.push_back(positions[i]);
            break;
        }
    }

    fitness += rangedFitness(ranged, opponents, area, maxDistance);
    fitness += meleeFitness(melee, opponents, maxDistance);
    fitness += healerFitness(healers, positions, opponents, maxDistance);
    return fitness;
}


float placementGA::rangedFitness(std::vector<vec2> positions, 
                                 std::vector<vec2> opponentLocations,
                                 std::vector<std::vector<int>> area,
                                 float maxDistance)
{
    float fitness = 0.0f;

    for(vec2 ranged : positions)
    {
        //Add area height to fitness, higher is a higher score
        fitness += area[ranged.x][ranged.y] * heightMOD_;

        //For each enemy remove the distance from the maxDistance.
        //Further away is a lower score.
        for(vec2 opponent : opponentLocations)
        {
            fitness += (maxDistance - distanceMh(ranged, opponent)) * enemyDistanceMODR_;
        }
    }

    return fitness;
}


float placementGA::meleeFitness(std::vector<vec2> positions, 
                                std::vector<vec2> opponentLocations,
                                float maxDistance)
{
    float fitness = 0.0f;

    //For each enemy remove the distance from the maxDistance
    //Further away is a lower score
    for(vec2 melee : positions)
    {
        for(vec2 opponent : opponentLocations)
        {
            fitness += (maxDistance - distanceMh(melee, opponent)) * enemyDistanceMODM_;
        }
    }

    return fitness;
}


float placementGA::healerFitness(std::vector<vec2> positions, 
                                 std::vector<vec2> allieLocations,
                                 std::vector<vec2> opponentLocations,
                                 float maxDistance)
{
    float fitness = 0.0f;

    for(vec2 healer : positions)
    {
        //For each team member remove the distance from the maxDistance
        //Further away is a lower score
        for(vec2 allie : allieLocations)
        {
            fitness += (maxDistance - distanceMh(healer, allie)) * healerRangeMOD_;
        }

        //For each enemy add the distance to the fitness
        //Further away is a higher score
        for(vec2 opponent : opponentLocations)
        {
            fitness += distanceMh(healer, opponent) * enemyDistanceMODH_;
        }
    }

    return fitness;
}

float placementGA::distanceMh(vec2 point1, vec2 point2)
{
    return(abs(point1.x - point2.x) + abs(point1.y - point2.y));
}