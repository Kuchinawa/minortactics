#pragma once

#include <glm\glm.hpp>
#include <random>
#include <vector>

#include "..\GameObjects\CharStatsComponent.h"
#include "..\GameObjects\GameObject.h"


using namespace glm;

namespace TacticEngine
{
    /*
    Genetic Algorithm that determines the best position for the given units.
    */
    class placementGA
    {
    public:

        std::vector<vec2> bestPositions;

        placementGA(
            std::vector<BattleClassType> unitTypes,
            std::vector<vec2> opponents,
            std::vector<std::vector<int>> area,
            int nrOfGenerations,
            int populationSize,
            int crossoverPercent,
            int mutatePercent,
            vec2 minPosition,
            vec2 maxPosition,
            float heightMOD = 5.0f,
            float healerRangeMOD = 0.75f,
            float enemyDistanceMODM = 1.0f,
            float enemyDistanceMODR = 0.75f,
            float enemyDistanceMODH = 0.50f);

        //Calculate the fitness score for unit positioning.
        float positioningFitness(
            std::vector<vec2> positions,
            std::vector<BattleClassType> unitTypes,
            std::vector<vec2> opponents,
            std::vector<std::vector<int>> area);
        //float maxDistance);

        void runGA();

    private:

        float heightMOD_;
        float healerRangeMOD_;
        float enemyDistanceMODM_;
        float enemyDistanceMODR_;
        float enemyDistanceMODH_;

        int nrOfGenerations_;
        int populationSize_;
        int crossoverPercent_;
        int mutatePercent_;
        int currentGeneration_;

        vec2 minPosition_;
        vec2 maxPosition_;

        typedef std::vector<vec2> chromosome;
        typedef std::vector<chromosome> population;

        population population_;
        std::vector<float> fitness_;

        std::vector<BattleClassType> unitTypes_;
        std::vector<vec2> opponents_;
        std::vector<std::vector<int>> area_;

        std::mt19937 randomMt19937;

        //Returns a randomly generated unitposition vector, all vectors are uniquely chosen from possiblePositions.
        std::vector<vec2> randomIndividual(std::vector<vec2> possiblePositions, int unitAmount);

        //Selection function
        void select();

        //Crossover function
        void crossover();

        //Mutation function
        void mutate(std::vector<vec2> possiblePositions);

        //Calculate fitness score
        void calcFitness();

        //Reports on the current scores
        void report();

        //Save the best score, switch worst with old best
        void elitism();

        //Returns the manhatten distance between the given points.
        float distanceMh(vec2 point1, vec2 point2);

        //Calculate the fitness score for ranged positioning.
        float rangedFitness(
            std::vector<vec2> positions, 
            std::vector<vec2> opponentLocations,
            std::vector<std::vector<int>> area,
            float maxDistance);

        //Calculate the firness score for warrior positioning.
        float meleeFitness(
            std::vector<vec2> positions, 
            std::vector<vec2> opponentLocations,
            float maxDistance);

        //Calculate the fitness score for healer positioning.
        float healerFitness(
            std::vector<vec2> positions, 
            std::vector<vec2> allieLocations,
            std::vector<vec2> opponentLocations,
            float maxDistance);
    };
}