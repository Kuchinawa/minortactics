#include "AttackingState.h"

#include "ControlStateContext.h"
#include "SelectState.h"

#include <iostream>

using namespace TacticEngine;

AttackingState::AttackingState(ActionTypes action) : ControlState()
{
    action_ = action;
}

void AttackingState::handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject)
{
    if(key == GLFW_MOUSE_BUTTON_1)
    {
        if(selectedGameObject != nullptr)
        {
            if(action_ == Attacking)
            {
                if(selectedGameObject->getObjectType() == ENEMYCHAR)
                {
                    currentContext->parentLevel->battle(currentContext->parentLevel->selectedGameObject, selectedGameObject, Attacking);

                    currentContext->setState(new SelectState());
                }
            }
            else if(action_ == Healing)
            {
                if(selectedGameObject != nullptr)
                {
                    //Select target location
                    if(selectedGameObject->getObjectType() == PLAYERCHAR || selectedGameObject->getObjectType() == ENEMYCHAR)
                    {
                        currentContext->parentLevel->battle(currentContext->parentLevel->selectedGameObject, selectedGameObject, Healing);

                        currentContext->setState(new SelectState());
                    }
                }
            }
        }
    }
}