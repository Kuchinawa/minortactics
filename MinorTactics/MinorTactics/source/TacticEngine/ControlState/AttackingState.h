#pragma once

#include "ControlState.h"
#include "..\GameObjects\CharStatsComponent.h"
#include "..\level.h"



namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class AttackingState : public ControlState
    {
    public:
        AttackingState(ActionTypes action);
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);
        
    private:
        ActionTypes action_;

    };
}