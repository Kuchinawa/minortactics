#pragma once

#include "..\Gameobjects\GameObject.h"

namespace TacticEngine
{
    class ControlStateContext;
}


namespace TacticEngine
{
    
    class ControlState
    {
    public:

        virtual void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject) = 0;
    };
}