#include "ControlStateContext.h"

#include "DefaultState.h"

using namespace TacticEngine;

ControlStateContext::ControlStateContext(level* pLevel)
{
    parentLevel = pLevel;
    setState(new DefaultState());
}

void ControlStateContext::setState(ControlState* newState)
{
    currentState = newState;
}

void ControlStateContext::handleState(int key, GameObject* selectedGameObject)
{
    currentState->handleState(this, key, selectedGameObject);
}