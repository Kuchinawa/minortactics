#pragma once

#include <GL/glew.h>
#include <GL/glfw.h>

#include "..\GameObjects\GameObject.h"

#include "ControlState.h"


namespace TacticEngine
{

    class level;

    class ControlStateContext
    {
    public:
        level* parentLevel;

        //Instantiate the StateContext with the default state.
        ControlStateContext(level* parentLevel = nullptr);

        //Set the current state
        void setState(ControlState* newState);

        //Handle current state
        void handleState(int key, GameObject* selectedGameObject);

    private:
        ControlState* currentState;
    };
}