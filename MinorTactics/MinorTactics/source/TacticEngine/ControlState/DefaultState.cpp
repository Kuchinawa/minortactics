#include "DefaultState.h"

#include "ControlStateContext.h"
#include "SelectState.h"
#include "EndTurnState.h"
#include "..\level.h"


#include <iostream>


using namespace TacticEngine;

void DefaultState::handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject)
{
    if(key == GLFW_MOUSE_BUTTON_1)
    {
        if(selectedGameObject != nullptr)
        {
            //Select character
            if(selectedGameObject->getObjectType() == PLAYERCHAR)
            {
                std::cout << selectedGameObject->getComponent<CharStatsComponent>()->name 
                    << " - " 
                    << selectedGameObject->getComponent<CharStatsComponent>()->getClassTypeString() 
                    << " Selected." <<std::endl << selectedGameObject->getComponent<CharStatsComponent>()->printStats();

                currentContext->parentLevel->selectedGameObject = selectedGameObject;
                currentContext->setState(new SelectState());
            }
            else if(selectedGameObject->getObjectType() == ENEMYCHAR)
            {
                std::cout << selectedGameObject->getComponent<CharStatsComponent>()->name 
                    << " - " 
                    << selectedGameObject->getComponent<CharStatsComponent>()->getClassTypeString() <<std::endl << selectedGameObject->getComponent<CharStatsComponent>()->printStats();
            }
        }
    }   
    else if(key == 'X')
    {
        //End the turn
        currentContext->setState(new EndTurnState());
        currentContext->parentLevel->endTurn();
    }
}