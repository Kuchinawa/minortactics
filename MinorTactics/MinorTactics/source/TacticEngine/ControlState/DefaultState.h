#pragma once

#include "ControlState.h"

namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class DefaultState : public ControlState
    {
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);
    };
}