#pragma once

#include "ControlState.h"
#include "..\level.h"

namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class EndTurnState : public ControlState
    {
    public:
        EndTurnState();
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);
    };
}