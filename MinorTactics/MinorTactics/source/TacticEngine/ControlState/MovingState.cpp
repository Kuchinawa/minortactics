#include "MovingState.h"

#include "ControlStateContext.h"

#include <iostream>

using namespace TacticEngine;

MovingState::MovingState() : ControlState()
{
    moving_ = false;
}

void MovingState::handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject)
{
    if(key == GLFW_MOUSE_BUTTON_1)
    {
        if (moving_ == false)
        {
            if(selectedGameObject != nullptr)
            {
                //Select target location
                if(selectedGameObject->getObjectType() == GROUND)
                {
                    if(!currentContext->parentLevel->pathFinder.getOccupied((int)selectedGameObject->getComponent<TransformComponent>()->location.x,
                        (int)selectedGameObject->getComponent<TransformComponent>()->location.z))
                    {
                        std::cout << "Moving to: " 
                            << (int)selectedGameObject->getComponent<TransformComponent>()->location.x << " "
                            << (int)selectedGameObject->getComponent<TransformComponent>()->location.z
                            << std::endl;

                        moving_ = true;
                        currentContext->parentLevel->moveSelectedCharacterTo(selectedGameObject);
                    }
                    else
                    {
                        std::cout << "Point is occupied" << std::endl;
                    }
                }
            }
        }
    }
}