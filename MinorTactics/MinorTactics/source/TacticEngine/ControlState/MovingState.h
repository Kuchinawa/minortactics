#pragma once

#include "ControlState.h"
#include "..\level.h"

namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class MovingState : public ControlState
    {
    public:
        MovingState();
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);

    private:
        bool moving_;
    };
}