#include "SelectState.h"

#include "AttackingState.h"
#include "ControlStateContext.h"
#include "DefaultState.h"
#include "EndTurnState.h"
#include "MovingState.h"
#include "..\level.h" 

#include <iostream>

using namespace TacticEngine;

void SelectState::handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject)
{
    if(key == GLFW_MOUSE_BUTTON_1)
    {   //Deselect Character
        if(selectedGameObject == nullptr || selectedGameObject->getObjectType() == GROUND)
        {
            std::cout << "Character deselected." << std::endl;
            currentContext->parentLevel->selectedGameObject = nullptr;
            currentContext->setState(new DefaultState());
            return;
        }
        else if(selectedGameObject->getObjectType() == PLAYERCHAR)
        {
            currentContext->setState(new DefaultState());
            currentContext->handleState(key, selectedGameObject);
            return;
        }
    }
    else if(key == 'M')
    {   
        if(!currentContext->parentLevel->selectedGameObject->getComponent<CharStatsComponent>()->moved)
        {
            //Move selected Character
            std::cout << "Select move location." << std::endl;
            currentContext->setState(new MovingState());
        }
        else
        {
            std::cout << "This unit already moved this turn!" << std::endl;
        }
    }
    else if(key == 'T')
    {   
        if(!currentContext->parentLevel->selectedGameObject->getComponent<CharStatsComponent>()->performedAction)
        {
            //Attack with the selected Character
            std::cout << "Select attack target." << std::endl;
            currentContext->setState(new AttackingState(Attacking));
        }
        else
        {
            std::cout << "This unit cannot perform anymore actions this turn!" << std::endl;
        }
    }
    else if(key == 'H')
    {   
        if(currentContext->parentLevel->selectedGameObject->getComponent<CharStatsComponent>()->classType == Priest)
        {
            if(!currentContext->parentLevel->selectedGameObject->getComponent<CharStatsComponent>()->performedAction)
            {
                //Heal with the selected Character
                std::cout << "Select healing target." << std::endl;
                currentContext->setState(new AttackingState(Healing));
            }
            else
            {
                std::cout << "This unit cannot perform anymore actions this turn!" << std::endl;
            }
        }
        else
        {
            std::cout << "This unit does not have healing capabilities!" << std::endl;
        }
    }
    else if(key == 'X')
    {
        std::cout << "Character deselected." << std::endl;
        currentContext->parentLevel->selectedGameObject = nullptr;

        std::cout << "Ending turn..." << std::endl;
        currentContext->setState(new EndTurnState());
        currentContext->parentLevel->endTurn();
    }
}