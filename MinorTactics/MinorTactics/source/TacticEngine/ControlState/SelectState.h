#pragma once

#include "ControlState.h"

namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class SelectState : public ControlState
    {
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);
    };
}