#pragma once

#include "..\GameObjects\GameObject.h"

#include "ControlState.h"

namespace TacticEngine
{
    class ControlStateContext;
}

namespace TacticEngine
{
    class WaitingState : public ControlState
    {
        void handleState(ControlStateContext* currentContext, int key, GameObject* selectedGameObject);
    };
}