#include "AIComponent.h"

//#include "CharStatsComponent.h"
#include "MovementComponent.h"
#include "..\level.h"

using namespace TacticEngine;

AIComponent::AIComponent(GameObject* parent) : Component()
{
    parent_ = parent;
    active = false;
    done = false;
    perfAction = false;
    actionTarget = nullptr;
}

void AIComponent::initialize()
{
}

void AIComponent::update(float secondsElapsed)
{
    if(active)
    {
        if(!parent_->getComponent<MovementComponent>()->isMoving())
        {
            if(perfAction)
            {
                performAction(actionType, actionTarget);
            }

            done = true;
            active = false;
        }
    }
}

void AIComponent::render()
{
}

void AIComponent::planAction(ActionTypes action, GameObject* target)
{
    perfAction = true;
    actionTarget = target;
    actionType = action;
}

void AIComponent::performAction(ActionTypes action, GameObject* target)
{
    if(action == Attacking)
    {
        perfAction = true;
        parent_->getLevel()->battle(parent_, target, Attacking);
    }
    else if(action == Healing)
    {
        perfAction = true;
        parent_->getLevel()->battle(parent_, target, Healing);
    }
}

void AIComponent::resetTurn()
{
    active = false;
    done = false;
    perfAction = false;
    actionTarget = nullptr;
}