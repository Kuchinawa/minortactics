#pragma once

#include "Component.h"
#include "GameObject.h"
#include "CharStatsComponent.h"

namespace TacticEngine
{
    /*
    Component that contains the AI status
    */
    class AIComponent : public Component
    {
    public:
        AIComponent(GameObject* parent);

        void initialize();
        void update(float secondsElapsed);
        void render();

        bool active;
        bool done;

        bool perfAction;
        GameObject* actionTarget;
        ActionTypes actionType;

        void planAction(ActionTypes action, GameObject* target);
        void performAction(ActionTypes action, GameObject* target);

        void resetTurn();

    private:
        GameObject* parent_;
    };
}