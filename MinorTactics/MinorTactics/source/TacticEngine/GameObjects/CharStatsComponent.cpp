#include "CharStatsComponent.h"

#include <iostream>
#include <glm\glm.hpp>


#include "TransformComponent.h"
#include "..\level.h"

#include <sstream>

using namespace TacticEngine;

CharStatsComponent::CharStatsComponent(GameObject* parent,
                                       std::string name,
                                       BattleClassType classType,
                                       int currentHealth, 
                                       int maxHealth,
                                       int attackRange,
                                       int attackPower,
                                       int attackHeight,
                                       int spellRange,
                                       int spellPower,
                                       int speed,
                                       int jumpHeight) : Component()
{
    this->name = name;
    this->classType = classType;
    this->parent_ = parent;
    this->currentHealth = currentHealth;
    this->maxHealth = maxHealth;
    this->attackRange = attackRange;
    this->attackPower = attackPower;
    this->attackHeight = attackHeight;
    this->spellRange = spellRange;
    this->spellPower = spellPower;
    this->speed = speed;
    this->jumpHeight = jumpHeight;
    this->isAlive = true;

    performedAction = false;
    moved = false;
}

std::string CharStatsComponent::getClassTypeString()
{
    const std::string classTypes[] = {"Warrior", "Archer", "Priest"};

    return classTypes[classType];
}

void CharStatsComponent::initialize()
{
}
void CharStatsComponent::update(float secondsElapsed)
{
}
void CharStatsComponent::render()
{
}

void CharStatsComponent::receiveDamage(int damage)
{
    if(currentHealth - damage <= 0)
    {
        currentHealth = 0;
        isAlive = false;

        parent_->getLevel()->pathFinder.setOccupied(parent_->getComponent<TransformComponent>()->get2DLocation().x, 
            parent_->getComponent<TransformComponent>()->get2DLocation().y, 
            false);
        std::cout <<  name << " dies! !(O_O;)" << std::endl;
    }
    else
    {
        currentHealth -= damage;
        std::cout <<  name << " receives "  << damage << " damage, current health at: " << currentHealth << "/" << maxHealth << "." << std::endl;
    }
}

void CharStatsComponent::attack(GameObject* target)
{
    //If within attack range
    if(attackRange >= distanceMh(parent_->getComponent<TransformComponent>()->get2DLocation(), target->getComponent<TransformComponent>()->get2DLocation()))
    {   //If not too high/low
        if(abs(parent_->getLevel()->pathFinder.getPointY(parent_->getComponent<TransformComponent>()->get2DLocation().x, parent_->getComponent<TransformComponent>()->get2DLocation().y)
            -
            target->getLevel()->pathFinder.getPointY(target->getComponent<TransformComponent>()->get2DLocation().x, target->getComponent<TransformComponent>()->get2DLocation().y))
            <= attackHeight)
        {

            std::cout << name << " - " << getClassTypeString() << " attacks " << target->getComponent<CharStatsComponent>()->name << std::endl;
            target->getComponent<CharStatsComponent>()->receiveDamage(attackPower);
            performedAction = true;
        }
        else
        {
            if(parent_->getLevel()->playerTurn) 
                std::cout << "Target unreachable! Height difference is too big." << std::endl;
        }
    }
    else
    {
        if(parent_->getLevel()->playerTurn) 
            std::cout << "Target out of range! Select a target within " << attackRange << " tiles." << std::endl;
    }
}

void CharStatsComponent::receiveHealing(int healing)
{
    if(currentHealth + healing >= maxHealth)
    {
        currentHealth = maxHealth;
    }
    else
    {
        currentHealth += healing;
    }

    std::cout <<  name << " is healed for "  << healing << ", current health at: " << currentHealth << "/" << maxHealth << "." << std::endl;
}

void CharStatsComponent::heal(GameObject* target)
{
    if(spellRange >= distanceMh(parent_->getComponent<TransformComponent>()->get2DLocation(), target->getComponent<TransformComponent>()->get2DLocation()))
    {
        std::cout << name << " - " << getClassTypeString() << " heals " << target->getComponent<CharStatsComponent>()->name << "." <<std::endl;
        target->getComponent<CharStatsComponent>()->receiveHealing(spellPower);
        performedAction = true;
    }
    else
    {
        if(parent_->getLevel()->playerTurn) 
            std::cout << "Target out of range! Select a target within " << spellRange << " tiles." << std::endl;
    }
}

void CharStatsComponent::resetTurn()
{
    performedAction = false;
    moved = false;
}

float CharStatsComponent::distanceMh(glm::vec2 point1, glm::vec2 point2)
{
    return(abs(point1.x - point2.x) + abs(point1.y - point2.y));
}

std::string CharStatsComponent::printStats()
{
    std::stringstream returnString;

    returnString << "Health: " << currentHealth << "/" << maxHealth << std::endl;
    returnString << "Attack PWR: " << attackPower << std::endl;
    returnString << "Spell PWR: " << spellPower << std::endl;
    returnString << "Speed: " << speed << std::endl;

    return returnString.str();
}