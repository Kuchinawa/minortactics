#pragma once

#include "Component.h"
#include "GameObject.h"

#include <string>

namespace TacticEngine
{
    enum BattleClassType { Warrior, Archer, Priest };
    enum ActionTypes { Attacking, Healing };

    /*
    Component that contains the characters status
    */
    class CharStatsComponent : public Component
    {
    public:
        std::string name;
        BattleClassType classType;
        int maxHealth;
        int currentHealth;
        int attackRange;
        int attackPower;
        int attackHeight;
        int spellRange;
        int spellPower;
        int speed;
        int jumpHeight;
        bool isAlive;

        CharStatsComponent(GameObject* parent, 
            std::string name,
            BattleClassType classType = Warrior,
            int currentHealth = 10, 
            int maxHealth = 10, 
            int attackRange = 1, 
            int attackPower = 5,
            int attackHeight = 1,
            int spellRange = 3,
            int spellPower = 0, 
            int speed = 3,
            int jumpHeight = 1);

        //Variables that keep track if the character moved this turn
        bool performedAction;
        bool moved;

        void initialize();
        void update(float secondsElapsed);
        void render();

        void receiveDamage(int damage);
        void receiveHealing(int healing);

        void attack(GameObject* target);
        void heal(GameObject* target);

        void resetTurn();

        std::string getClassTypeString();
        std::string CharStatsComponent::printStats();

    private:
        GameObject* parent_;

        float distanceMh(glm::vec2 point1, glm::vec2 point2);
    };
}