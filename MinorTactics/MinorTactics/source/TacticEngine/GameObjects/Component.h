#pragma once

namespace TacticEngine
{
    /*
     Component base class, all components inherit this class.
    */
    class Component
    {

    public:
        virtual void initialize(){};
        virtual void update(float secondsElapsed){};
        virtual void render(){};
    };
}