#include <memory>
#include <vector>

#include "GameObject.h"
#include "TransformComponent.h"

#include "..\level.h"

using namespace TacticEngine;

GameObject::GameObject(glm::vec3 spawnLocation, GameObjectType objectType, level* parent)
{
    std::unique_ptr<TransformComponent> transformComponent (new TransformComponent(this, spawnLocation));
    GameObject::addComponent(std::move(transformComponent));

    this->objectType_ = objectType;
    this->parentLevel_ = parent;
}

GameObject::~GameObject()
{

}

void GameObject::addComponent(std::unique_ptr<Component> component)
{
    //Add the component to the components_ map using the type as key
    components_[&typeid(*component.get())] = std::move(component);
}

GameObjectType GameObject::getObjectType()
{
    return objectType_;
}

level* GameObject::getLevel()
{
    return parentLevel_;
}
