#pragma once
#include <memory>
#include <vector>
#include <unordered_map>
#include <type_traits>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Component.h"

#include "..\Graphics\Program.h"
#include "..\Graphics\Texture.h"

#include "..\UI\Camera.h"

namespace TacticEngine
{
    enum GameObjectType { GROUND, PLAYERCHAR, ENEMYCHAR };

    class level;

    class GameObject
    {
    public:

        GameObject(glm::vec3 spawnLocation, GameObjectType objectType, level* parent);
        ~GameObject();

        void initialize()
        {
            //Loop through all the attached components and call their initialize method
            //second == the KV value
            for(auto &component : components_)
            {
                component.second->initialize();
            }
        }

        void update(float secondsElapsed)
        {
            //Loop through all the attached components and call their update method
            //second == the KV value
            for(auto &component : components_)
            {
                component.second->update(secondsElapsed);
            }
        }

        void render()
        {
            //Loop through all the attached components and call their render method
            //second == the KV value
            for(auto &component : components_)
            {
                component.second->render();
            }
        }

        //Add a component to the components_ map
        void addComponent(std::unique_ptr<Component> component);

        //Get the component based on type T
        template <typename T>
        T* getComponent()
        {
            if (components_.count(&typeid(T)) != 0)
            {
                return static_cast<T*>(components_[&typeid(T)].get());
            }
            else
            {
                return nullptr;
            }
        }

        
        //Get the GameObjectType
        GameObjectType getObjectType();

        level* getLevel();
    private:
        std::unordered_map<const std::type_info*, std::unique_ptr<Component>> components_;

        GameObjectType objectType_;

        level* parentLevel_;
    };
}