#include "MovementComponent.h"

#include "..\ControlState\SelectState.h"
#include "..\level.h"

using namespace TacticEngine;

MovementComponent::MovementComponent(GameObject* parent) : Component()
{
    parent_ = parent;
    movementSpeed_ = 2.0f;
    moved_ = 0.0f;
}

void MovementComponent::startMoving(std::vector<ASPoint*> path)
{
    path_ = path;
    originalPosition_ = parent_->getComponent<TransformComponent>()->location;

    if(!path_.empty())
    {
        if(path_.size() > parent_->getComponent<CharStatsComponent>()->speed)
            path_.erase(path_.begin(), path_.end() - parent_->getComponent<CharStatsComponent>()->speed);
        parent_->getLevel()->pathFinder.setOccupied(originalPosition_.x, originalPosition_.z, false);
        parent_->getLevel()->pathFinder.setOccupied(path_[0]->getX(), path_[0]->getZ(), true);
    }
}

void MovementComponent::initialize()
{
}

void MovementComponent::update(float secondsElapsed)
{
    if(!path_.empty())
    {
        glm::vec3 currentLocation = parent_->getComponent<TransformComponent>()->location;

        float distance = secondsElapsed * movementSpeed_;

        //More then one point
        while(moved_ + distance >= 1.0f && !path_.empty())
        {
            //Finish current point
            currentLocation = path_.back()->getPosition();
            distance -= (1.0f - moved_);
            moved_ = 0.0f;
            path_.pop_back();
        }


        if(distance > 0.0f && !path_.empty())
        {
            //Move towards the direction of the current waypoint
            currentLocation += distance * glm::normalize(path_.back()->getPosition() - currentLocation);
            moved_ += distance;
        }
        
        parent_->getComponent<TransformComponent>()->setPosition(currentLocation);

        if(path_.empty())
        {
            std::cout << parent_->getComponent<CharStatsComponent>()->name << " finished moving." << std::endl;

            if(parent_->getLevel()->playerTurn)
            {
                parent_->getLevel()->controlState.setState(new SelectState());
            }
        }
    }
}

bool MovementComponent::isMoving()
{
    return !path_.empty();
}


void MovementComponent::render()
{
}