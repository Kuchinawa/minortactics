#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

#include "Component.h"
#include "GameObject.h"
#include "TransformComponent.h"

#include "..\AI\ASPoint.h"

namespace TacticEngine
{
    /*
    Component that handles grid based movement
    */
    class MovementComponent : public Component
    {
    public:
        MovementComponent(GameObject* parent);

        void initialize();
        void update(float secondsElapsed);
        void render();

        void startMoving(std::vector<ASPoint*> path);

        bool isMoving();

    private:
        GameObject* parent_;
        
        glm::vec3 originalPosition_;
        
        std::vector<ASPoint*> path_;
        float movementSpeed_;
        float moved_;
        
    };
}