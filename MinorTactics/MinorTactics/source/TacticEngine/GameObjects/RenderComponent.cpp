#include "RenderComponent.h"
#include "Component.h"
#include "GameObject.h"
#include "TransformComponent.h"

#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace TacticEngine;


RenderComponent::RenderComponent(GameObject* parent, Program* program, Texture* texture, Camera* camera, std::string objPath) : Component()
{
    //Initialize private variables
    parent_ = parent;
    program_ = program;
    texture_ = texture;
    camera_ = camera;

    //Load model or default to block
    if(objPath.empty() || !objModel_.loadModel(objPath, program_))
    {
        // make and bind the VAO
        glGenVertexArrays(1, &VAO_);
        glBindVertexArray(VAO_);

        // make and bind the VBO
        glGenBuffers(1, &VBO_);
        glBindBuffer(GL_ARRAY_BUFFER, VBO_);

        // Put the triangle vertices (XYZ) and texture coordinates (UV) into the VBO
        GLfloat vertexData[] = {
            //  X     Y     Z       U     V
            // bottom
            -0.5f,-0.5f,-0.5f,   0.0f, 0.0f,
            0.5f,-0.5f,-0.5f,   1.0f, 0.0f,
            -0.5f,-0.5f, 0.5f,   0.0f, 1.0f,
            0.5f,-0.5f,-0.5f,   1.0f, 0.0f,
            0.5f,-0.5f, 0.5f,   1.0f, 1.0f,
            -0.5f,-0.5f, 0.5f,   0.0f, 1.0f,

            // top
            -0.5f, 0.5f,-0.5f,   0.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,   0.0f, 1.0f,
            0.5f, 0.5f,-0.5f,   1.0f, 0.0f,
            0.5f, 0.5f,-0.5f,   1.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,   0.0f, 1.0f,
            0.5f, 0.5f, 0.5f,   1.0f, 1.0f,

            // front
            -0.5f,-0.5f, 0.5f,   1.0f, 0.0f,
            0.5f,-0.5f, 0.5f,   0.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,   1.0f, 1.0f,
            0.5f,-0.5f, 0.5f,   0.0f, 0.0f,
            0.5f, 0.5f, 0.5f,   0.0f, 1.0f,
            -0.5f, 0.5f, 0.5f,   1.0f, 1.0f,

            // back
            -0.5f,-0.5f,-0.5f,   0.0f, 0.0f,
            -0.5f, 0.5f,-0.5f,   0.0f, 1.0f,
            0.5f,-0.5f,-0.5f,   1.0f, 0.0f,
            0.5f,-0.5f,-0.5f,   1.0f, 0.0f,
            -0.5f, 0.5f,-0.5f,   0.0f, 1.0f,
            0.5f, 0.5f,-0.5f,   1.0f, 1.0f,

            // left
            -0.5f,-0.5f, 0.5f,   0.0f, 1.0f,
            -0.5f, 0.5f,-0.5f,   1.0f, 0.0f,
            -0.5f,-0.5f,-0.5f,   0.0f, 0.0f,
            -0.5f,-0.5f, 0.5f,   0.0f, 1.0f,
            -0.5f, 0.5f, 0.5f,   1.0f, 1.0f,
            -0.5f, 0.5f,-0.5f,   1.0f, 0.0f,

            // right
            0.5f,-0.5f, 0.5f,   1.0f, 1.0f,
            0.5f,-0.5f,-0.5f,   1.0f, 0.0f,
            0.5f, 0.5f,-0.5f,   0.0f, 0.0f,
            0.5f,-0.5f, 0.5f,   1.0f, 1.0f,
            0.5f, 0.5f,-0.5f,   0.0f, 0.0f,
            0.5f, 0.5f, 0.5f,   0.0f, 1.0f
        };


        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

        // connect the xyz to the "vert" attribute of the vertex shader
        glEnableVertexAttribArray(program_->attrib("vert"));
        glVertexAttribPointer(program_->attrib("vert"), 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), nullptr);

        // connect the uv coords to the "vertTexCoord" attribute of the vertex shader
        glEnableVertexAttribArray(program_->attrib("vertTexCoord"));
        glVertexAttribPointer(program_->attrib("vertTexCoord"), 2, GL_FLOAT, GL_TRUE,  5*sizeof(GLfloat), (const GLvoid*)(3 * sizeof(GLfloat)));

        // unbind the VBO and VAO
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
}

void RenderComponent::initialize()
{
    //link to the parents Transform component so the location/transformation variables can be located
    transform_ = parent_->getComponent<TransformComponent>();
}

void RenderComponent::update(float secondsElapsed)
{
}

void RenderComponent::render()
{
    if(objModel_.loaded)
    {
        objModel_.renderModel(program_, texture_, camera_, transform_->getMatrix());
    }
    else
    {
        // bind the program (the shaders)
        glUseProgram(program_->object());

        // bind the VAO
        glBindVertexArray(VAO_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_->object());
        program_->setUniform("tex", 0); //set to 0 because the texture is bound to GL_TEXTURE0

        //set the camera uniform
        program_->setUniform("camera", camera_->matrix());

        //set the model uniform
        program_->setUniform("model", transform_->getMatrix());

        // draw the VAO
        glDrawArrays(GL_TRIANGLES, 0, 6*2*3);

        // unbind the VAO
        glBindVertexArray(0);

        // unbind the program
        glUseProgram(0);
    }

}

void RenderComponent::setTexture(Texture* texture)
{
    texture_ = texture;
}