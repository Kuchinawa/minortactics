#pragma once

#include <memory>

#include "Component.h"
#include "GameObject.h"
#include "TransformComponent.h"

#include "..\Graphics\OBJModel.h"
#include "..\Graphics\Program.h"
#include "..\Graphics\Texture.h"
#include "..\UI\Camera.h"

namespace TacticEngine
{
    /*
    Component that renders the parent object, this component requires a TransformComponent
    */
    class RenderComponent : public Component
    {
    public:

        RenderComponent(GameObject* parent, Program* program, Texture* texture, Camera* camera, std::string objPath = "");

        void initialize();
        void update(float secondsElapsed);
        void render();

        void setTexture(Texture* texture);

    private:
        GameObject* parent_;
        TransformComponent* transform_;

        OBJModel objModel_;

        GLuint VAO_;
        GLuint VBO_;

        Program* program_;
        Texture* texture_;
        Camera* camera_;
    };
}