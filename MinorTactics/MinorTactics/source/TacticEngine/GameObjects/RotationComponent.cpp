#include "RotationComponent.h"
#include "Component.h"
#include "TransformComponent.h"
#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace TacticEngine;

RotationComponent::RotationComponent(GameObject* parent) : Component()
{
    parent_ = parent;
}

void RotationComponent::initialize()
{
}

void RotationComponent::update(float secondsElapsed)
{
    const GLfloat degreesPerSecond = 180.0f;

    //Rotate by 1 degree
    degreesRotated_ += secondsElapsed * degreesPerSecond;

    //Don't go over 360 degress
    while(degreesRotated_ > 360.0f) 
        degreesRotated_ -= 360.0f;

    parent_->getComponent<TransformComponent>()->rotation = glm::rotate(glm::mat4(), degreesRotated_, glm::vec3(0,1,0));
}

void RotationComponent::render()
{

}