#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Component.h"
#include "GameObject.h"


namespace TacticEngine
{
    /*
     Component that makes the parent rotate
    */
    class RotationComponent : public Component
    {
    public:
        glm::mat4 rotation;

        RotationComponent(GameObject* parent);

        void initialize();
        void update(float secondsElapsed);
        void render();

    private:
        GameObject* parent_;
        float degreesRotated_;
    };

}