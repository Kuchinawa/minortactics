#include "SelectableComponent.h"

using namespace TacticEngine;

SelectableComponent::SelectableComponent(GameObject* parent, glm::vec3 boundingBoxMin, glm::vec3 boundingBoxMax) : Component()
{
    parent_ = parent;
    boundingBox_.push_back(boundingBoxMin);
    boundingBox_.push_back(boundingBoxMax);
}

std::vector<glm::vec3> SelectableComponent::getBoundingBox()
{
    glm::vec3 location = transform_->location;
    std::vector<glm::vec3> rBoundingBox;
    rBoundingBox.push_back(location + boundingBox_[0]);
    rBoundingBox.push_back(location + boundingBox_[1]);

    return rBoundingBox;
}

void SelectableComponent::initialize()
{
    //link to the parents Transform component so the location/transformation variables can be located
    transform_ = parent_->getComponent<TransformComponent>();
}

void SelectableComponent::update(float secondsElapsed)
{
}

void SelectableComponent::render()
{

}