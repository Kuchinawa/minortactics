#pragma once

#include "Component.h"
#include "GameObject.h"
#include "TransformComponent.h"

namespace TacticEngine
{
    /*
     Component that makes the parent object selectable by adding a boundingbox
     requires a TransformComponent.
    */
    class SelectableComponent : public Component
    {
    public:

        SelectableComponent(GameObject* parent, glm::vec3 boundingBoxMin, glm::vec3 boundingBoxMax);

        //Returns a vector containing 2 Vec3s that make up the objects boundingbox
        std::vector<glm::vec3> getBoundingBox();

        void initialize();
        void update(float secondsElapsed);
        void render();

    private:
        GameObject* parent_;
        TransformComponent* transform_;

        std::vector<glm::vec3> boundingBox_;
    };
}