#include "TransformComponent.h"

using namespace TacticEngine;

TransformComponent::TransformComponent(GameObject* parent, glm::vec3 spawnLocation) : Component()
{
    _parent = parent;
    location = spawnLocation;
    objectMatrix = glm::translate(glm::mat4(), spawnLocation);
    rotation = glm::mat4(1.0f);//glm::rotate(glm::mat4(1.0f), 0.0f, glm::vec3(0,1.0f,0) );
    scaling = glm::scale(glm::vec3(1.0f));
}

void TransformComponent::initialize()
{

}

void TransformComponent::update(float secondsElapsed)
{
}

void TransformComponent::render()
{

}

void TransformComponent::move(glm::vec3 displacement)
{
    objectMatrix += glm::translate(displacement);
    location += displacement;
}

void TransformComponent::setPosition(glm::vec3 newPosition)
{
    objectMatrix = glm::translate(glm::mat4(), newPosition);
    location = newPosition;
}

glm::vec2 TransformComponent::get2DLocation()
{
    return glm::vec2((int)location.x, (int)location.z);
}

void TransformComponent::scale(glm::vec3 amount)
{
    scaling = glm::scale(amount);
}

void TransformComponent::rotate(float angle, glm::vec3 axis)
{
    rotation = glm::rotate(angle, axis);
}

glm::mat4 TransformComponent::getMatrix()
{
    if(_parent->getObjectType() == PLAYERCHAR || _parent->getObjectType() == ENEMYCHAR)
    {
        return (glm::translate(glm::vec3(0.0f, 0.50f, 0.0f)) * objectMatrix * rotation * scaling);
    }
    return objectMatrix * rotation * scaling;
}