#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <memory>

#include "Component.h"
#include "GameObject.h"

#include "..\UI\Camera.h"

namespace TacticEngine
{
    /*
     Component that keeps track of all the positioning data of the parent
    */
    class TransformComponent : public Component
    {
    public:
        glm::vec3 location;
        glm::mat4 rotation;
        glm::mat4 scaling;
        glm::mat4 objectMatrix;
        TransformComponent(GameObject* parent, glm::vec3 spawnLocation);

        void scale(glm::vec3 amount);
        void rotate(float angle, glm::vec3 axis);

        void initialize();
        void update(float secondsElapsed);
        void render();

        glm::mat4 getMatrix();

        //Move the GameObject with the displacement vector3
        void move(glm::vec3 displacement);
        void setPosition(glm::vec3 newPosition);

        glm::vec2 get2DLocation();

    private:
        GameObject* _parent;
    };
}