#include "OBJModel.h"

#include <vector>
#include <glm\glm.hpp>
#include <cstring>
#include <sstream>

using namespace TacticEngine;

OBJModel::OBJModel()
{
    loaded = false;
}

bool OBJModel::loadModel(std::string sFileName, Program* program)
{
    std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
    std::vector< glm::vec3 > temp_vertices;
    std::vector< glm::vec2 > temp_uvs;
    std::vector< glm::vec3 > temp_normals;

    FILE* file = fopen(sFileName.c_str(), "r");
    if(file == nullptr)
    {
        printf("Cant load OBJ file\n");
        return false;
    }
    iNumFaces = 0;
    while(true)
    {
        char lineHeader[128];
        //Read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if(res == EOF)
            break; // EOF == End Of File

        if(strcmp(lineHeader, "v") == 0)
        {
            glm::vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
            temp_vertices.push_back(vertex);
        }
        else if ( strcmp( lineHeader, "vt" ) == 0 )
        {
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            temp_uvs.push_back(uv);
        }
        else if ( strcmp( lineHeader, "vn" ) == 0 )
        {
            glm::vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            temp_normals.push_back(normal);
        }
        else if ( strcmp( lineHeader, "f" ) == 0 ){
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
            if (matches != 9){
                printf("File can't be read by our simple parser : ( Try exporting with other options\n");
                return false;
            }
            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices    .push_back(uvIndex[0]);
            uvIndices    .push_back(uvIndex[1]);
            uvIndices    .push_back(uvIndex[2]);
            normalIndices.push_back(normalIndex[0]);
            normalIndices.push_back(normalIndex[1]);
            normalIndices.push_back(normalIndex[2]);
            iNumFaces++;
        }

    }

    // For each vertex of each triangle
    for( unsigned int i=0; i<vertexIndices.size(); i++ )
    {
        unsigned int vertexIndex = vertexIndices[i];
        glm::vec3 vertex = temp_vertices[ vertexIndex-1 ];
        vertices.push_back(vertex);
    }

    for(unsigned int i=0;i<uvIndices.size(); i++)
    {
        unsigned int uvIndex = uvIndices[i];
        glm::vec2 uv = temp_uvs[uvIndex - 1];
        uvs.push_back(uv);
    }

    //Make and bind VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    //Make and bind vbo
    glGenBuffers(1, &VBO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(program->attrib("vert"));
    glVertexAttribPointer(program->attrib("vert"), 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glGenBuffers(1, &UVBuffer_);
    glBindBuffer(GL_ARRAY_BUFFER, UVBuffer_);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(program->attrib("vertTexCoord"));
    glVertexAttribPointer(program->attrib("vertTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, (void*)0);


    loaded = true;



    return true;
}

void OBJModel::renderModel(Program* program, Texture* texture, Camera* camera, glm::mat4 modelMatrix)
{
    if(!loaded)
        return;

    glDisable(GL_CULL_FACE);
    //Bind program (shaders)
    glUseProgram(program->object());

    //Bind VAO
    glBindVertexArray(VAO);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->object());
    program->setUniform("tex", 0);  //set to 0 because the texture is bound to GL_TEXTURE0

    //set the camera uniform
    program->setUniform("camera", camera->matrix());

    //Set the model uniform
    program->setUniform("model", modelMatrix);

    //Draw the VAO
    glDrawArrays(GL_TRIANGLES, 0, iNumFaces*3);

    //Unbind the VAO & Program
    glBindVertexArray(0);
    glUseProgram(0);
    glEnable(GL_CULL_FACE);
}