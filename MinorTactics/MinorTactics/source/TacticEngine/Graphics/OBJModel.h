#pragma once

#include <Windows.h>
#include "..\UI\Camera.h"
#include "Program.h"
#include "Texture.h"

namespace TacticEngine
{
    class OBJModel
    {
    public:

        OBJModel();

        bool loaded;
        bool loadModel(std::string sFileName, Program* program);

        void renderModel(Program* program, Texture* texture, Camera* camera, glm::mat4 modelMatrix);

    private:
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> uvs;

        int iNumFaces;

        UINT VBO_;
        UINT UVBuffer_;
        UINT VAO;
    };
}