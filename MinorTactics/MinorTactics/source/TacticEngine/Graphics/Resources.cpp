#include "Resources.h"

#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>

using namespace TacticEngine;

// returns the full path to the file `fileName` in the resources directory of the app bundle
std::string Resources::ResourcePath(std::string fileName)
{
    char executablePath[1024] = {'\0'};
    DWORD charsCopied = GetModuleFileName(nullptr, executablePath, 1024);
    if(charsCopied > 0 && charsCopied < 1024)
        return std::string(executablePath) + "\\..\\" + fileName;
    else
        throw std::runtime_error("GetModuleFileName failed a bit");
}

// returns a new TacticEngine::Texture created from the given filename
TacticEngine::Texture* Resources::LoadTexture(const char* filename)
{
    TacticEngine::Bitmap bmp = TacticEngine::Bitmap::bitmapFromFile(ResourcePath(filename));
    bmp.flipVertically();
    return new TacticEngine::Texture(bmp);
}

// returns a new TacticEngine:Program created from the given vertex and fragment shader filenames
TacticEngine::Program* Resources::LoadShaders(const char* vertFilename, const char* fragFilename)
{
    std::vector<TacticEngine::Shader> shaders;
    shaders.push_back(TacticEngine::Shader::shaderFromFile(Resources::ResourcePath(vertFilename), GL_VERTEX_SHADER));
    shaders.push_back(TacticEngine::Shader::shaderFromFile(Resources::ResourcePath(fragFilename), GL_FRAGMENT_SHADER));

    return new TacticEngine::Program(shaders);
}