#pragma once
// third-party libraries
#include <windows.h>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// standard C++ libraries
#include <cassert>
#include <iostream>
#include <stdexcept>
#include <cmath>


// TacticEngine classes
#include "Program.h"
#include "Texture.h"

namespace TacticEngine
{
    /*
     This class contains resource management methods for textures and shaders
    */
    class Resources
    {
    public:
        static TacticEngine::Texture* LoadTexture(const char* filename);
        static TacticEngine::Program* LoadShaders(const char* vertFilename, const char* fragFilename);
        static std::string ResourcePath(std::string fileName);
        
    };
}