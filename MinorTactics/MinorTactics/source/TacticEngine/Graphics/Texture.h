/*
TacticEngine::Texture

Based on the Texture files from htpp://tomdalling.com/
*/

#pragma once

#include <GL/glew.h>
#include <GL/glfw.h>
#include "Bitmap.h"

namespace TacticEngine
{
    enum ETextureFiltering
    {
        TEXTURE_FILTER_MAG_NEAREST = 0, // Nearest criterion for magnification
        TEXTURE_FILTER_MAG_BILINEAR, // Bilinear criterion for magnification
        TEXTURE_FILTER_MIN_NEAREST, // Nearest criterion for minification
        TEXTURE_FILTER_MIN_BILINEAR, // Bilinear criterion for minification
        TEXTURE_FILTER_MIN_NEAREST_MIPMAP, // Nearest criterion for minification, but on closest mipmap
        TEXTURE_FILTER_MIN_BILINEAR_MIPMAP, // Bilinear criterion for minification, but on closest mipmap
        TEXTURE_FILTER_MIN_TRILINEAR, // Bilinear criterion for minification on two closest mipmaps, then averaged
    };


    /**
    Represents an OpenGL texture
    */
    class Texture
    {
    public:
        /**
        Creates a texture from a bitmap.

        The texture will be loaded upside down because tdogl::Bitmap pixel data
        is ordered from the top row down, but OpenGL expects the data to
        be from the bottom row up.

        @param bitmap  The bitmap to load the texture from
        @param minMagFiler  GL_NEAREST or GL_LINEAR
        @param wrapMode GL_REPEAT, GL_MIRRORED_REPEAT, GL_CLAMP_TO_EDGE, or GL_CLAMP_TO_BORDER
        */
        Texture(const Bitmap& bitmap,
            GLint minMagFiler = GL_LINEAR,
            GLint wrapMode = GL_CLAMP_TO_EDGE);

        Texture();
        /**
        Deletes the texture object with glDeleteTextures
        */
        ~Texture();

        /**
        @result The texure object, as created by glGenTextures
        */
        GLuint object() const;

        /**
        @result The original width (in pixels) of the bitmap this texture was made from
        */
        GLfloat originalWidth() const;

        /**
        @result The original height (in pixels) of the bitmap this texture was made from
        */
        GLfloat originalHeight() const;

    private:
        GLuint _object;
        GLfloat _originalWidth;
        GLfloat _originalHeight;

        //copying disabled
        Texture(const Texture&);
        const Texture& operator=(const Texture&);
    };

}
