#include "lineDraw.h"

using namespace TacticEngine;

lineDraw::lineDraw()
{
    drawAble = false;
}

lineDraw::lineDraw(glm::vec3 origin, glm::vec3 end, Program* program,  Camera* camera)
{
    program_ = program;
    camera_  = camera;

    glGenVertexArrays(1, &VAO_);
    glBindVertexArray(VAO_);

    glGenBuffers(1, &VBO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);

    GLfloat vertexData[] = { 
        origin.x, origin.y, origin.z,
        end.x, end.y, end.z };

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(program_->attrib("vert"));
    glVertexAttribPointer(program_->attrib("vert"), 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), nullptr);

    drawAble = true;
}

void lineDraw::render()
{
    glUseProgram(program_->object());
    glBindVertexArray(VAO_);

    program_->setUniform("camera", camera_->matrix());

    glDrawArrays(GL_LINES, 0, 2);

    glBindVertexArray(0);

    glUseProgram(0);
}