
#include <glm/glm.hpp>

#include "..\Graphics\Program.h"
#include "..\Graphics\Texture.h"
#include "..\UI\Camera.h"

namespace TacticEngine
{
    class lineDraw
    {
    public:
        bool drawAble;

        lineDraw();
        lineDraw(glm::vec3 origin, glm::vec3 end, Program* program, Camera* camera);
        void render();
    private:
        glm::vec3 origin_;
        glm::vec3 end_;

        GLuint VAO_;
        GLuint VBO_;

        Program* program_;
        Camera* camera_;
    };
}