#include "Collision3D.h"
#include "../Physics/Picking.h"

#include <utility>
#include <vector>

#define RIGHT	0
#define LEFT	1
#define MIDDLE	2

using namespace glm;

namespace TacticEngine
{
    bool wooRayAABBoxIntersection(vec3 min, vec3 max, vec3 rayOrigin, vec3 rayDirection, double &distance)
    {

        bool inside = true;
        int quadrant[3];
        int i;
        int whichPlane;

        vec3 maxT;
        vec3 candidatePlane;
        vec3 coord;

        distance = 0.0;

        /* Find candidate planes.
        This loop can be avoided if rays cast all from the eye (perspective view) */
        for(i = 0; i < 3; i++)
        {
            if(rayOrigin[i] < min[i])
            {
                quadrant[i] = LEFT;
                candidatePlane[i] = min[i];
                inside = false;
            }
            else if(rayOrigin[i] > max[i])
            {
                quadrant[i] = RIGHT;
                candidatePlane[i] = max[i];
                inside = false;
            }
            else
            {
                quadrant[i] = MIDDLE;
            }
        }

        /* Ray is inside bounding box */
        if(inside)
        {
            coord = rayOrigin;
            return true;
        }

        /* Calculate the distances to candidate planes */
        for(i = 0; i < 3; i++)
        {
            if(quadrant[i] != MIDDLE && rayDirection[i] != 0.0)
                maxT[i] = (candidatePlane[i] - rayOrigin[i]) / rayDirection[i];
            else
                maxT[i] = -1.0;
        }

        /* Get largest of the maxT's for final choice of intersection */
        whichPlane = 0;
        for(i = 1; i < 3; i++)
        {
            if(maxT[whichPlane] < maxT[i])
            {
                whichPlane = i;
            }
        }
        /* Check if final candidate is inside box */
        if(maxT[whichPlane] < 0.0)
        {
            return false;
        }

        for(i = 0; i < 3; i++)
        {
            if(whichPlane != i)
            {
                coord[i] = rayOrigin[i] + maxT[whichPlane] * rayDirection[i];
                if(coord[i] < min[i] || coord[i] > max[i])
                {
                    return false; /* outside box */
                }
            }
            else
            {
                coord[i] = candidatePlane[i];
            }
        }

        //distance = max[whichPlane];
        distance = glm::distance(rayOrigin, coord);
        return true; /* ray hits box */
    }
}