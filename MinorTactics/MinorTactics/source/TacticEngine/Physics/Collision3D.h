#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace TacticEngine
{
    //Check if a ray collides with an Axis-Alligned box
    //minExtent is the boundingbox corner with the lowest value and maxExtent is the opposite
    bool wooRayAABBoxIntersection(glm::vec3 min, glm::vec3 max, glm::vec3 rayOrigin, glm::vec3 rayDirection, double &distance);
}