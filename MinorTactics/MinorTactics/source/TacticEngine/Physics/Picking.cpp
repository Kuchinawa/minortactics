#include "Picking.h"

namespace TacticEngine
{
    void PickingRay::setRay(float mouseX, float mouseY, Camera camera, GLint viewport[])
    {
        //Calculate the 3D position of the 2D click position, x position of the mouse is inverted because of openGLs origin
        _clickPosInWorld = glm::unProject(glm::vec3(mouseX, viewport[3]-mouseY, 0.0f), 
            camera.view(), 
            camera.projection(), 
            glm::vec4(viewport[0], viewport[1], viewport[2], viewport[3]));

        _direction = glm::normalize(_clickPosInWorld - camera.position());

    }

    glm::vec3 PickingRay::getClickPosInWorld()
    {
        return _clickPosInWorld;
    }

    glm::vec3 PickingRay::getDirection()
    {
        return _direction;
    }
}