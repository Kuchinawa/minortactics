#pragma once

#include "..\UI\Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include <iostream>

namespace TacticEngine
{
    class PickingRay
    {
    public:
        //Calculates the 3D position and direction of the 2D mouse location
        //and sets the corresponding vectors
        void setRay(float mouseX, float mouseY, Camera camera, GLint viewport[]);

        //Returns the calculated 3D position of the ray
        glm::vec3 getClickPosInWorld();

        //Returns the direction of the ray
        glm::vec3 getDirection();

    private:
        glm::vec3 _clickPosInWorld;
        glm::vec3 _direction;
        glm::vec3 _invDirection;

        glm::vec3 _screenHorizontal;
        glm::vec3 _screenVertical;
        glm::vec3 _view;
    };
}