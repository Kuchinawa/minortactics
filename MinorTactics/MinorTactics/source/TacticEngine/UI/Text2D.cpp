#include "Text2D.h"

//using namespace TacticEngine;

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

#include "..\Graphics\Program.h"

GLuint Text2DTextureID;
GLuint Text2DVertexBufferID;
GLuint Text2DUVBufferID;

//GLuint Text2DShaderID;
TacticEngine::Program* Text2DProgram = nullptr;
GLuint Text2DUniformID;
GLuint Text2DUniformColorID;

GLuint loadTGA(const char * imagepath)
{
    // Create one OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    // Read the file, call glTexImage2D with the right parameters
    glfwLoadTexture2D(imagepath, 0);

    // Nice trilinear filtering.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
    glGenerateMipmap(GL_TEXTURE_2D);

    // Return the ID of the texture we just created
    return textureID;
}


void initText2D(const char * texturePath){

    // Initialize texture
    Text2DTextureID = loadTGA(texturePath);

    // Initialize VBO
    glGenBuffers(1, &Text2DVertexBufferID);
    glGenBuffers(1, &Text2DUVBufferID);

    // Initialize Shader
    //Text2DShaderID = LoadShaders( "TextVertexShader.vertexshader", "TextVertexShader.fragmentshader" );
    Text2DProgram = TacticEngine::Resources::LoadShaders("TextShader.vertexshader", "TextShader.fragmentshader");

    // Initialize uniforms' IDs
    Text2DUniformID = glGetUniformLocation(Text2DProgram->object(), "myTextureSampler");
    Text2DUniformColorID = glGetUniformLocation(Text2DProgram->object(), "textColor");

}
void printText2D(const char * text, int x, int y, int size, glm::vec4 color)
{

    unsigned int length = strlen(text);

    // Fill buffers
    std::vector<glm::vec2> vertices;
    std::vector<glm::vec2> UVs;
    for ( unsigned int i=0 ; i<length ; i++ )
    {

        glm::vec2 vertex_up_left    = glm::vec2( x+i*size     , y+size );
        glm::vec2 vertex_up_right   = glm::vec2( x+i*size+size, y+size );
        glm::vec2 vertex_down_right = glm::vec2( x+i*size+size, y      );
        glm::vec2 vertex_down_left  = glm::vec2( x+i*size     , y      );

        vertices.push_back(vertex_up_left   );
        vertices.push_back(vertex_down_left );
        vertices.push_back(vertex_up_right  );

        vertices.push_back(vertex_down_right);
        vertices.push_back(vertex_up_right);
        vertices.push_back(vertex_down_left);

        char character = text[i];
        float uv_x = (character%16)/16.0f;
        float uv_y = (character/16)/16.0f;

        glm::vec2 uv_up_left    = glm::vec2( uv_x           , 1.0f - uv_y );
        glm::vec2 uv_up_right   = glm::vec2( uv_x+1.0f/16.0f, 1.0f - uv_y );
        glm::vec2 uv_down_right = glm::vec2( uv_x+1.0f/16.0f, 1.0f - (uv_y + 1.0f/16.0f) );
        glm::vec2 uv_down_left  = glm::vec2( uv_x           , 1.0f - (uv_y + 1.0f/16.0f) );
        UVs.push_back(uv_up_left   );
        UVs.push_back(uv_down_left );
        UVs.push_back(uv_up_right  );

        UVs.push_back(uv_down_right);
        UVs.push_back(uv_up_right);
        UVs.push_back(uv_down_left);
    }

    glBindBuffer(GL_ARRAY_BUFFER, Text2DVertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, Text2DUVBufferID);
    int q = vertices.size() * sizeof(glm::vec2);
    int r = sizeof(vertices);
    //glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(glm::vec2), &UVs[0], GL_STATIC_DRAW);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(glm::vec2), &UVs, GL_STATIC_DRAW);

    //Bind shader
    glUseProgram(Text2DProgram->object());

    //Bind texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Text2DTextureID);
    //glUniform1i(Text2DUniformID, 0);
    Text2DProgram->setUniform("myTextureSampler", 0);

    //1st Attribute buffer: vertices
    glEnableVertexAttribArray(Text2DProgram->attrib("vertexPosition_screenspace"));
    glBindBuffer(GL_ARRAY_BUFFER, Text2DVertexBufferID);
    glVertexAttribPointer(Text2DProgram->attrib("vertexPosition_screenspace"), 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    //2nd attribute buffer: UVs
    glEnableVertexAttribArray(Text2DProgram->attrib("vertexUV"));
    glBindBuffer(GL_ARRAY_BUFFER, Text2DUVBufferID);
    glVertexAttribPointer(Text2DProgram->attrib("vertexUV"), 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Draw call
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());

    //glDisable(GL_BLEND);

    //glDisableVertexAttribArray(0);
    //glDisableVertexAttribArray(1);
}


void cleanupText2D()
{

    // Delete buffers
    glDeleteBuffers(1, &Text2DVertexBufferID);
    glDeleteBuffers(1, &Text2DUVBufferID);

    // Delete texture
    glDeleteTextures(1, &Text2DTextureID);

    // Delete shader
    Text2DProgram->~Program();

}