#pragma once

#include "../Graphics/Resources.h"

void initText2D(const char * texturePath);
void printText2D(const char * text, int x, int y, int size, glm::vec4 color);
void cleanupText2D();