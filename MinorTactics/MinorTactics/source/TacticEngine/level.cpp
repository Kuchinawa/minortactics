#include "level.h"

#include <chrono>
#include <math.h>

#include "AI\OpponentAI.h"
#include "GameObjects\AIComponent.h"
#include "ControlState\DefaultState.h"
#include "ControlState\SelectState.h"

using namespace TacticEngine;

level::level()
{
}

void level::initialize(Camera* camera)
{
    selectedGameObject = nullptr;
    camera_ = camera;
    pathFinder = PathFinder();
    controlState = ControlStateContext(this);
    turn = 1;
    playerTurn = true;
    AI_ = new OpponentAI(&enemyCharacters_, &playerCharacters_, this);
}

level::~level(void)
{
    delete AI_;
}

void level::createTerrain(Program* blockProgram, Texture* blockTexture, int dimensionX, int dimensionY, int dimensionZ)
{
    //Set the terrain_ vector dimensions
    terrain_.resize(dimensionX);

    for(int plane = 0; plane < terrain_.size(); plane++)
        terrain_[plane].resize(dimensionY);

    //Spawn blocks
    //TODO: spawn blocks based on layout array?
    for (float plane = 0; plane < dimensionX; plane++)
    {
        for (float row = 0; row < dimensionY; row++)
        {
            for (float block = 0; block < dimensionZ; block++)
            {
                terrain_[plane][row].push_back(CreateBox(blockProgram, blockTexture, glm::vec3(plane, row, block)));
            }
        }
    }

}

void level::createTerrainFromLevelFile(Program* blockProgram, Texture* blockTexture, const char* levelFilename)
{
    std::ifstream mapFile;

    //Open the file
    mapFile.open(Resources::ResourcePath(levelFilename));

    if(!mapFile.is_open())
        throw std::runtime_error(std::string("Failed to open file: ") + "test.level");

    //Get the dimensions
    int rowSize, colSize;
    mapFile >> rowSize >> colSize;

    //Resize the height map vector
    std::vector<std::vector<int>> levelMap;
    levelMap.resize(rowSize);

    //Load the height map
    for(int row = 0; row < rowSize; row++)
    {
        for(int col = 0; col < colSize; col++)
        {
            int height;
            mapFile >> height;
            levelMap[row].push_back(height);
        }
    }

    //Set the terrain_ vector dimensions
    terrain_.resize(levelMap.size());

    for(int row = 0; row < terrain_.size(); row++)
        terrain_[row].resize(levelMap[row].size());

    //Spawn blocks inside the terrain vector
    for(float row = 0; row < levelMap.size(); row++)
    {
        for(float collumn = 0; collumn < levelMap[row].size(); collumn++)
        {
            for(float block = 0; block < levelMap[row][collumn]; block++)
            {
                terrain_[row][collumn].push_back(CreateBox(blockProgram, blockTexture, glm::vec3(row, block, collumn)));
                pathFinder.addTerrain(terrain_[row][collumn].back());
            }
        }
    }
}

GameObject* level::CreateBox(Program* blockProgram, Texture* blockTexture, glm::vec3 spawnLocation)
{
    GameObject* newBox = new GameObject(spawnLocation, GROUND, this);
    newBox->addComponent(std::unique_ptr<RenderComponent>(new RenderComponent(newBox, blockProgram, blockTexture, camera_)));
    newBox->addComponent(std::unique_ptr<SelectableComponent>(new SelectableComponent(newBox, glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.5f, 0.5f, 0.5f))));
    newBox->initialize();

    return newBox;
}

void level::spawnCharacter(glm::vec3 location, std::string name, BattleClassType classType, GameObjectType charType, Program* program, Texture* texture, Camera* camera, glm::vec3 scaling, float rotation, std::string objPath)
{
    //Add character components
    GameObject* character = new GameObject(location, charType, this);
    character->addComponent(std::unique_ptr<RenderComponent>(new RenderComponent(character, program, texture, camera, objPath)));
    character->addComponent(std::unique_ptr<SelectableComponent>(new SelectableComponent(character, glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.5f, 0.5f, 0.5f))));

    if(charType == ENEMYCHAR)
    {
        character->addComponent(std::unique_ptr<AIComponent>(new AIComponent(character)));
    }

    //Add the class associated stats
    if(classType == Warrior)
        character->addComponent(std::unique_ptr<CharStatsComponent>(new CharStatsComponent(character, name, classType, 10, 10, 1, 5, 1, 3, 0, 3, 1)));
    else if(classType == Priest)
        character->addComponent(std::unique_ptr<CharStatsComponent>(new CharStatsComponent(character, name, classType,  5,  5, 3, 2, 1, 3, 3, 3, 1)));
    else if(classType == Archer)
        character->addComponent(std::unique_ptr<CharStatsComponent>(new CharStatsComponent(character, name, classType,  5,  5, 3, 4, 3, 3, 0, 3, 2)));

    character->addComponent(std::unique_ptr<MovementComponent>(new MovementComponent(character)));
    character->initialize();

    //Add character to the appropriate character vector
    if(charType == PLAYERCHAR)
    {
        playerCharacters_.push_back(character);
    }
    else if(charType == ENEMYCHAR)
    {
        enemyCharacters_.push_back(character);
    }

    character->getComponent<TransformComponent>()->scale(scaling);
    character->getComponent<TransformComponent>()->rotate(rotation, glm::vec3(0.0f, 1.0f, 0.0f));

    //Set the location as occupied
    pathFinder.setOccupied(location.x, location.z, true);
}

void level::update(float secondsElapsed)
{
    //Update all the characters
    for(GameObject* character : playerCharacters_)
        character->update(secondsElapsed);

    for(GameObject* character : enemyCharacters_)
        character->update(secondsElapsed);

    if(!playerTurn)
    {
        AI_->update();

        if(AI_->finished())
        {
            endTurn();
        }
    }
}

void level::render()
{
    for (int plane = 0; plane < terrain_.size(); plane++)
        for (int row = 0; row < terrain_[plane].size(); row++)
            for (int block = 0; block < terrain_[plane][row].size(); block++)
                terrain_[plane][row][block]->render();

    for (int c = 0; c < playerCharacters_.size(); c++)
    {
        if(playerCharacters_[c]->getComponent<CharStatsComponent>()->isAlive)
            playerCharacters_[c]->render();
    }

    for (int c = 0; c < enemyCharacters_.size(); c++)
    {
        if(enemyCharacters_[c]->getComponent<CharStatsComponent>()->isAlive)
            enemyCharacters_[c]->render();
    }
}

GameObject* level::selectObject(PickingRay &pickingRay)
{
    GameObject* clickedGameObject = nullptr;
    double shortestDistance;
    double newDistance;
    std::vector<GameObject*> collisionVector;

    bool first = true;

    //Loop through selectable objects
    for (int plane = 0; plane < terrain_.size(); plane++)
    {
        for (int row = 0; row < terrain_[plane].size(); row++)
        {
            for (GameObject* block : terrain_[plane][row])
            {   //Check if the ray collides with the boundingbox of the GameObject
                std::vector<glm::vec3> boundingBox = block->getComponent<SelectableComponent>()->getBoundingBox();
                if(wooRayAABBoxIntersection(boundingBox[0], boundingBox[1], pickingRay.getClickPosInWorld(), pickingRay.getDirection(), newDistance))
                {
                    //std::cout << block->getComponent<TransformComponent>()->location.x << " " << 
                    //    block->getComponent<TransformComponent>()->location.y << " " << 
                    //    block->getComponent<TransformComponent>()->location.z << " " << 
                    //    newDistance << std::endl;
                    if(first)
                    {
                        shortestDistance = newDistance;
                        clickedGameObject = block;
                        first = false;
                    }
                    else if(newDistance < shortestDistance)
                    {
                        shortestDistance = newDistance;
                        clickedGameObject = block;
                    }
                }
            }
        }
    }

    for(GameObject* character : playerCharacters_)
    {   
        if(character->getComponent<CharStatsComponent>()->isAlive)
        {
            //Check if the ray collides with the boundingbox of the GameObject
            std::vector<glm::vec3> boundingBox = character->getComponent<SelectableComponent>()->getBoundingBox();
            if(wooRayAABBoxIntersection(boundingBox[0], boundingBox[1], pickingRay.getClickPosInWorld(), pickingRay.getDirection(), newDistance))
            {
                //std::cout << character->getComponent<TransformComponent>()->location.x << " " << 
                //    character->getComponent<TransformComponent>()->location.y << " " << 
                //    character->getComponent<TransformComponent>()->location.z << " " << 
                //    newDistance << std::endl;
                if(first)
                {
                    shortestDistance = newDistance;
                    clickedGameObject = character;
                    first = false;
                }
                else if(newDistance < shortestDistance)
                {
                    shortestDistance = newDistance;
                    clickedGameObject = character;
                }
            }
        }
    }

    for(GameObject* character : enemyCharacters_)
    {   
        if(character->getComponent<CharStatsComponent>()->isAlive)
        {
            //Check if the ray collides with the boundingbox of the GameObject
            std::vector<glm::vec3> boundingBox = character->getComponent<SelectableComponent>()->getBoundingBox();
            if(wooRayAABBoxIntersection(boundingBox[0], boundingBox[1], pickingRay.getClickPosInWorld(), pickingRay.getDirection(), newDistance))
            {
                if(first)
                {
                    shortestDistance = newDistance;
                    clickedGameObject = character;
                    first = false;
                }
                else if(newDistance < shortestDistance)
                {
                    shortestDistance = newDistance;
                    clickedGameObject = character;
                }
            }
        }
    }

    return clickedGameObject;
}

void level::moveSelectedCharacterTo(GameObject* groundTarget)
{
    std::vector<ASPoint*> path = pathFinder.aStar((int)selectedGameObject->getComponent<TransformComponent>()->location.x, 
        (int)selectedGameObject->getComponent<TransformComponent>()->location.z,
        (int)groundTarget->getComponent<TransformComponent>()->location.x,
        (int)groundTarget->getComponent<TransformComponent>()->location.z,
        selectedGameObject->getComponent<CharStatsComponent>()->jumpHeight);

    if(path.size() <= selectedGameObject->getComponent<CharStatsComponent>()->speed)
    {
        //Move the selected game object to the target position using the A* algorithm
        selectedGameObject->getComponent<MovementComponent>()->startMoving(path);

        selectedGameObject->getComponent<CharStatsComponent>()->moved = true;
    }
    else
    {
        std::cout << "Location to far! Select a location thats within " << selectedGameObject->getComponent<CharStatsComponent>()->speed << " tiles." << std::endl;
        controlState.setState(new SelectState());
    }
    ////Set occupied
    //pathFinder.setOccupied((int)selectedGameObject->getComponent<TransformComponent>()->location.x, 
    //    (int)selectedGameObject->getComponent<TransformComponent>()->location.z, 
    //    false);
    //pathFinder.setOccupied((int)groundTarget->getComponent<TransformComponent>()->location.x,
    //    (int)groundTarget->getComponent<TransformComponent>()->location.z,
    //    true);
}

void level::battle(GameObject* attacker, GameObject* defender, ActionTypes action)
{
    if(action == Attacking)
    {
        attacker->getComponent<CharStatsComponent>()->attack(defender);
    }
    else if(action == Healing)
    {
        attacker->getComponent<CharStatsComponent>()->heal(defender);
    }

    bool pAlive = false;
    for(GameObject* character : playerCharacters_)
    {
        if(pAlive == false && character->getComponent<CharStatsComponent>()->isAlive)
        {
            pAlive = true;
        }
    }

    bool eAlive = false;
    for(GameObject* character : enemyCharacters_)
    {
        if(eAlive == false && character->getComponent<CharStatsComponent>()->isAlive)
        {
            eAlive = true;
        }
    }

    if(pAlive == false && eAlive == true)
    {
        std::cout << "You lose!" << std::endl;

        glfwCloseWindow();
        glfwTerminate();
        std::cin.ignore();
        exit(EXIT_SUCCESS);
    }
    else if(pAlive == true && eAlive == false)
    {
        std::cout << "You win!" << std::endl;

        glfwCloseWindow();
        glfwTerminate();
        std::cin.ignore();
        exit(EXIT_SUCCESS);
    }
    else if(pAlive == false && eAlive == false)
    {
        std::cout << "Draw.... how???" << std::endl;

        glfwCloseWindow();
        glfwTerminate();
        std::cin.ignore();
        exit(EXIT_SUCCESS);
    }
}


void level::handleControls(int key, GameObject* target)
{
    controlState.handleState(key, target);
}

void level::calculatePositions()
{
    int nrOfGenerations = 3000;
    int populationSize = 100;

    vec2 minPosition(0,0);
    vec2 maxPosition(4,9);

    std::vector<vec2> alliePos;
    std::vector<BattleClassType> unitTypes;
    std::vector<vec2> opponentPos;

    //Get allie positions and types
    for(GameObject* pChar : playerCharacters_)
    {
        alliePos.push_back(vec2(pChar->getComponent<TransformComponent>()->location.x,
            pChar->getComponent<TransformComponent>()->location.z));
        unitTypes.push_back(pChar->getComponent<CharStatsComponent>()->classType);
    }

    //Get enemy positions
    for(GameObject* eChar : enemyCharacters_)
    {
        opponentPos.push_back(vec2(eChar->getComponent<TransformComponent>()->location.x,
            eChar->getComponent<TransformComponent>()->location.z));
    }

    placementGA placement(unitTypes, opponentPos, pathFinder.getGrid(), nrOfGenerations, populationSize, 600, 1, minPosition, maxPosition);
    placement.runGA();

    for(int i = 0; i < placement.bestPositions.size(); i++)
    {
        float x = placement.bestPositions[i].x;
        float y = pathFinder.getPointY(placement.bestPositions[i].x, placement.bestPositions[i].y);
        float z = placement.bestPositions[i].y;
        playerCharacters_[i]->getComponent<TransformComponent>()->setPosition(vec3(x,y,z));
    }
}

glm::vec2 level::getTerrainDimensions()
{
    return vec2(terrain_.size(), terrain_[0].size());
}

void level::endTurn()
{
    if(playerTurn)
    {
        std::cout << "Ending turn..." << std::endl;
        playerTurn = false;

        for(GameObject* character : enemyCharacters_)
        {
            character->getComponent<CharStatsComponent>()->resetTurn();
            character->getComponent<AIComponent>()->resetTurn();
        }
    }
    else
    {
        playerTurn = true;

        for(GameObject* character : playerCharacters_)
        {
            character->getComponent<CharStatsComponent>()->resetTurn();
        }

        controlState.setState(new DefaultState());
        ++turn;
        std::cout << "------------------" << std::endl;
        std::cout << "Start of turn " << turn << std::endl;
        std::cout << "------------------" << std::endl;
    }
}