#pragma once

#include <vector>
#include <fstream>

#include "AI\placementGA.h"
#include "AI\PathFinder.h"
#include "AI\asPoint.h"


#include "ControlState\ControlStateContext.h"

#include "Physics\Collision3D.h"
#include "Physics\Picking.h"

#include "Graphics\Resources.h"

#include "GameObjects\GameObject.h"
#include "GameObjects\CharStatsComponent.h"
#include "GameObjects\MovementComponent.h"
#include "GameObjects\RenderComponent.h"
#include "GameObjects\SelectableComponent.h"

namespace TacticEngine
{
    class OpponentAI;
}

namespace TacticEngine
{
    class level
    {
    public:
        level();
        ~level(void);

        GameObject* selectedGameObject;
        ControlStateContext controlState;
        PathFinder pathFinder;

        int turn;
        bool playerTurn;

        //Calculate the best positions for the enemy units
        void calculatePositions();

        //Generate the terrain blocks
        void createTerrain(Program* blockProgram, Texture* blockTexture, int dimensionX, int dimensionY, int dimensionZ);

        //Generate the terrain blocks from a level file
        void createTerrainFromLevelFile(Program* blockProgram, Texture* blockTexture, const char* levelFilename);

        //Get the XY dimensions of the terrain
        glm::vec2 getTerrainDimensions();

        //Spawn a character gameobject in the level
        void spawnCharacter(glm::vec3 location, std::string name, BattleClassType classType, GameObjectType charType, Program* program, Texture* texture, Camera* camera, glm::vec3 scaling = glm::vec3(1.0f), float rotation = 0.0f, std::string objPath = "");

        //Select a GameObject using a PickingRay
        GameObject* selectObject(PickingRay &pickingRay);

        //Move the currently selected character to the Coords above the groundTarget
        void moveSelectedCharacterTo(GameObject* groundTarget);

        //Make two GameObjects do battle
        void battle(GameObject* attacker, GameObject* defender, ActionTypes action);

        //End the turn
        void endTurn();

        //Sends the used key and a GameObject as target
        void handleControls(int key, GameObject* target);

        void initialize(Camera* camera);

        void update(float secondsElapsed);

        void render();

    private:

        std::vector<std::vector<std::vector<GameObject*>>> terrain_;

        std::vector<GameObject*> playerCharacters_;
        std::vector<GameObject*> enemyCharacters_;

        Camera* camera_;


        OpponentAI* AI_;


        //Generate block GameObjects
        GameObject* level::CreateBox(Program* blockProgram, Texture* blockTexture, glm::vec3 spawnLocation);
    };
}