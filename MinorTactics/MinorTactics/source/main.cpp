﻿#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifdef _DEBUG   
#ifndef DBG_NEW      
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )      
#define new DBG_NEW   
#endif
#endif  // _DEBUG
// standard C++ libraries
#include <cassert>
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <list>
#include <memory>

// third-party libraries
#include <windows.h>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// TacticEngine classes
#include "TacticEngine\Graphics\Program.h"
#include "TacticEngine\Graphics\Texture.h"
#include "TacticEngine\Graphics\Resources.h"
#include "TacticEngine\UI\Camera.h"

#include "TacticEngine\GameObjects\GameObject.h"
#include "TacticEngine\GameObjects\Component.h"
#include "TacticEngine\GameObjects\RenderComponent.h"
#include "TacticEngine\GameObjects\CharStatsComponent.h"


//#include "TacticEngine\UI\Text2D.h"

#include "TacticEngine\level.h"

#include "TacticEngine\Physics\Picking.h"
#include "TacticEngine\Physics\Collision3D.h"

#include "TacticEngine\Graphics\lineDraw.h"

void Cleanup();

using namespace TacticEngine;

// constants
const glm::vec2 SCREEN_SIZE(800, 600);
GLint viewport[4];

//Toggle Genetic Algorithm
bool GAON = false;

//globals

//Orthographic projection matrix for 2D rendering (recalc when resizing window)
glm::mat4 ortho = glm::ortho(0.0f, float(SCREEN_SIZE.x), 0.0f, float(SCREEN_SIZE.y)); 

//Shader programs
Program* gBoxProgram = nullptr;
Program* primitiveProgram = nullptr;
Program* modelProgram = nullptr;

//(3D) Textures
Texture* gBoxTexture = nullptr;
Texture* knightModelTexture = nullptr;
Texture* archerModelTexture = nullptr;
Texture* priestModelTexture = nullptr;

Camera gCamera;

level testLevel;

int gMouseX, gMouseY = 0;
float fps;

PickingRay pickingRay;

lineDraw line;

//Update a single frame
static void Update(float secondsElapsed)
{
    //Update objects
    testLevel.update(secondsElapsed);

    //move position of the camera based on the WASD/QE keys
    const float moveSpeed = 10.0;

    if(glfwGetKey('S'))
        gCamera.offsetPosition(secondsElapsed * moveSpeed * -glm::vec3(gCamera.forward().x, 0, gCamera.forward().z));
    if(glfwGetKey('W'))
        gCamera.offsetPosition(secondsElapsed * moveSpeed * glm::vec3(gCamera.forward().x, 0, gCamera.forward().z));

    if(glfwGetKey('A'))
        gCamera.offsetPosition(secondsElapsed * moveSpeed * -gCamera.right());
    if(glfwGetKey('D'))
        gCamera.offsetPosition(secondsElapsed * moveSpeed * gCamera.right());

    if(glfwGetKey('Q'))
    {
        glm::mat4 rotation;
        rotation = glm::rotate(rotation, secondsElapsed * moveSpeed * 10.0f, gCamera.up());
        glm::vec3 focus = gCamera.position() + (10.0f * gCamera.forward());
        glm::vec3 camFocusVector = gCamera.position() - focus;
        camFocusVector = glm::vec3(rotation * glm::vec4(camFocusVector, 1.0f));
        camFocusVector += focus;

        gCamera.setPosition(camFocusVector);
        gCamera.lookAt(focus);
    }
    if(glfwGetKey('E'))
    {
        glm::mat4 rotation;
        rotation = glm::rotate(rotation, secondsElapsed * -moveSpeed * 10.0f, gCamera.up());
        glm::vec3 focus = gCamera.position() + (10.0f * gCamera.forward());
        glm::vec3 camFocusVector = gCamera.position() - focus;
        camFocusVector = glm::vec3(rotation * glm::vec4(camFocusVector, 1.0f));
        camFocusVector += focus;

        gCamera.setPosition(camFocusVector);
        gCamera.lookAt(focus);
    }
}

//Mouse click handler
void GLFWCALL MouseCallback(int key, int action)
{
    if(key == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS)
    {
        glfwGetMousePos(&gMouseX, &gMouseY);
        //std::cout << "Click at: "<< gMouseX << " " << gMouseY << std::endl;

        //Shoot picking ray from the clicked location
        pickingRay.setRay((float)gMouseX, (float)gMouseY, gCamera, viewport);

        //set debug line
        line = lineDraw(pickingRay.getClickPosInWorld(), pickingRay.getClickPosInWorld() + (pickingRay.getDirection() * 100.0f), primitiveProgram, &gCamera);

        //Send the key and clicked GameObject to the control state machine
        testLevel.handleControls(key, testLevel.selectObject(pickingRay));
    }
}

void GLFWCALL MouseWheelCallBack(int pos)
{
    //increase or decrease field of view based on mouse wheel
    const float zoomSensitivity = -2.0;
    float fieldOfView = gCamera.fieldOfView() + zoomSensitivity * (float)glfwGetMouseWheel();
    if(fieldOfView < 5.0f)
        fieldOfView = 5.0f;
    if(fieldOfView > 130.0f)
        fieldOfView = 130.0f;
    gCamera.setFieldOfView(fieldOfView);
    glfwSetMouseWheel(0);
}


//Keyboard Handler
void GLFWCALL KeyboardCallback(int key, int action)
{
    if((key == 'M' || key == 'T' || key == 'X' || key == 'H') && action == GLFW_PRESS)
    {
        testLevel.handleControls(key, nullptr);
    }
}

//draws a single frame
static void Render()
{
    // clear everything
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    testLevel.render();


    //char text[256];
    //sprintf(text,"%.0f fps", fps );
    //printText2D(text, 10, 500, 60, glm::vec4(1.0f,1.0f,1.0f,1.0f));

    if(line.drawAble)
        line.render();

    // swap the display buffers (displays what was just drawn)
    glfwSwapBuffers();
}


void AppMain()
{
    std::cout << "Run Genetic Algorithm? (y/n)" << std::endl;
    std::string geneticAlgOn;
    std::cin >> geneticAlgOn;

    if(geneticAlgOn == "y")
        GAON = true;

    // initialise GLFW
    if(!glfwInit())
        throw std::runtime_error("glfwInit failed");

    glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
    glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE,GL_TRUE);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    if(!glfwOpenWindow((int)SCREEN_SIZE.x, (int)SCREEN_SIZE.y, 8, 8, 8, 8, 16, 0, GLFW_WINDOW))
        throw std::runtime_error("glfwOpenWindow failed. Can your hardware handle OpenGL 3.2?");

    // Initialize GLEW
    glewExperimental = GL_TRUE; //Stops glew crashing on OSX
    if (glewInit() != GLEW_OK)
        throw std::runtime_error("Failed to initialize GLEW\n");

    glClearColor(0.0f, 0.0f, 0.2f, 0.0f); // blue

    //Enable Depth buffering
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);

    // print out some info about the graphics drivers
    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "------------------" << std::endl;

    // make sure OpenGL version 3.2 API is available
    if(!GLEW_VERSION_3_2)
        throw std::runtime_error("OpenGL 3.2 API is not available.");

    //GLFW Settings
    glfwSetWindowTitle( "MinorTactics" );
    glfwEnable(GLFW_STICKY_KEYS); // Ensure we can capture the escape key being pressed below
    //glfwDisable(GLFW_MOUSE_CURSOR);
    glfwSetMouseWheel(0);

    glfwSetMouseButtonCallback(MouseCallback);
    glfwSetMouseWheelCallback(MouseWheelCallBack);
    glfwSetKeyCallback(KeyboardCallback);

    glfwSetWindowPos(500, 100);
    glGetIntegerv(GL_VIEWPORT, viewport);

    //Load vertex and fragment shaders
    //gFontProgram = Resources::LoadShaders("font2D.vertexshader", "font2D.fragmentshader");
    gBoxProgram = Resources::LoadShaders("camera.vertexshader", "camera.fragmentshader");
    primitiveProgram = Resources::LoadShaders("primitive.vertexshader", "primitive.fragmentshader");
    modelProgram = Resources::LoadShaders("model.vertexshader", "model.fragmentshader");

    std::string priestOBJ = Resources::ResourcePath("priest.obj");
    std::string knightOBJ = Resources::ResourcePath("knight.obj");
    std::string archerOBJ = Resources::ResourcePath("archer.obj");

    //Load the textures
    gBoxTexture = Resources::LoadTexture("white.jpg");
    knightModelTexture = Resources::LoadTexture("knight.png");
    archerModelTexture = Resources::LoadTexture("archer.png");
    priestModelTexture = Resources::LoadTexture("priest.png");


    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Initialize text library with the font
    //initText2D(Resources::ResourcePath("ExportedFont.tga").c_str());

    //Set default camera position
    gCamera.setPosition(glm::vec3(-5.0f, 10.0f, -5.0f));
    gCamera.setAngles(glm::vec2(135, 36));
    gCamera.setViewportAspectRatio(SCREEN_SIZE.x / SCREEN_SIZE.y);

    //Initialize the level
    testLevel.initialize(&gCamera);
    //testLevel.createTerrain(gBoxProgram, gBoxTexture, 10, 3, 10);
    testLevel.createTerrainFromLevelFile(gBoxProgram, gBoxTexture, "test.level");


    float modelScale = 00.10f;
    float modelRotation = 90.0f;

    testLevel.spawnCharacter(glm::vec3(3,3,4), "Kuchinawa", Warrior, PLAYERCHAR, modelProgram, knightModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, knightOBJ);
    testLevel.spawnCharacter(glm::vec3(3,3,5), "Lloyd", Warrior, PLAYERCHAR, modelProgram, knightModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, knightOBJ);
    testLevel.spawnCharacter(glm::vec3(1,3,4), "Raine", Priest, PLAYERCHAR, modelProgram, priestModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, priestOBJ);
    testLevel.spawnCharacter(glm::vec3(1,3,5), "Colette", Priest, PLAYERCHAR, modelProgram, priestModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, priestOBJ);
    testLevel.spawnCharacter(glm::vec3(3,5,2), "Chester", Archer, PLAYERCHAR, modelProgram, archerModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, archerOBJ);
    testLevel.spawnCharacter(glm::vec3(3,5,7), "Natalia", Archer, PLAYERCHAR, modelProgram, archerModelTexture, &gCamera, glm::vec3(modelScale), modelRotation, archerOBJ);

    testLevel.spawnCharacter(glm::vec3(6,3,4), "Zidane", Warrior, ENEMYCHAR, modelProgram, knightModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, knightOBJ);
    testLevel.spawnCharacter(glm::vec3(6,3,5), "Squall", Warrior, ENEMYCHAR, modelProgram, knightModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, knightOBJ);
    testLevel.spawnCharacter(glm::vec3(8,3,4), "Garnet", Priest, ENEMYCHAR, modelProgram, priestModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, priestOBJ);
    testLevel.spawnCharacter(glm::vec3(8,3,5), "Rinoa", Priest, ENEMYCHAR, modelProgram, priestModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, priestOBJ);
    testLevel.spawnCharacter(glm::vec3(6,5,2), "Vivi", Archer, ENEMYCHAR, modelProgram, archerModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, archerOBJ);
    testLevel.spawnCharacter(glm::vec3(6,5,7), "Selphie", Archer, ENEMYCHAR, modelProgram, archerModelTexture, &gCamera, glm::vec3(modelScale), -modelRotation, archerOBJ);

    if(GAON)
    {
        testLevel.calculatePositions();
    }

    pickingRay = PickingRay();

    std::cout << "Welcome to MinorTactics!" << std::endl;
    std::cout << "Controls:" << std::endl;
    std::cout << "WASD/QE are used to control the camera." << std::endl;
    std::cout << "Left click to select." << std::endl;
    std::cout << "Press M to move." << std::endl;
    std::cout << "Press T to attack." << std::endl;
    std::cout << "Press H to heal." << std::endl;
    std::cout << "Press X to end the turn." << std::endl;
    std::cout << "------------------" << std::endl;
    std::cout << "Start of turn " << testLevel.turn << std::endl;
    std::cout << "------------------" << std::endl;

    //Run while the window is open
    double lastTime = glfwGetTime();
    double lastfpsTime = glfwGetTime();
    int nbFrames = 0;
    while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ))
    {
        //Measure speed
        double currentTime = glfwGetTime();
        nbFrames++;

        if ( currentTime - lastfpsTime >= 1.0 )// If last prinf() was more than 1 sec ago
        { 
            // printf and reset timer
            //std::cout << (1000.0/(1000.0/double(nbFrames))) << " fps\n"<< std::endl;
            fps = (1000.0/(1000.0/double(nbFrames)));
            nbFrames = 0;
            lastfpsTime += 1.0;
        }


        //Update the scene based on the time elapsed since the last update
        Update(currentTime - lastTime);
        lastTime = currentTime;

        //Draw frame
        Render();
    }


    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    Cleanup();

}

void Cleanup()
{
    delete gBoxProgram;
    delete primitiveProgram;
    delete modelProgram;

    delete gBoxTexture;
    delete knightModelTexture;
    delete archerModelTexture;
    delete priestModelTexture;
}

int main(int argc, char *argv[]) {

    try 
    {
        AppMain();
    }
    catch (const std::exception& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl;
        std::cerr << "Exiting..." << std::endl;
        std::cin.ignore();
        return EXIT_FAILURE;
    }
    _CrtDumpMemoryLeaks();
    //return EXIT_SUCCESS;
}